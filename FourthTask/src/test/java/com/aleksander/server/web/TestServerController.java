package com.aleksander.server.web;

import com.aleksander.server.core.factory.PersonFactory;
import com.aleksander.server.core.model.Person;
import com.aleksander.server.core.respository.PersonRepositoryImpl;
import com.aleksander.server.core.services.PersonService;
import com.aleksander.server.core.services.PersonServiceImpl;
import com.aleksander.server.core.services.ValidationServiceImpl;
import com.aleksander.server.web.model.PersonRequestModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class TestServerController {

    private PersonService personService;
    private ServerController serverController;



    @Before
    public void init() {
        personService = new PersonServiceImpl(new PersonFactory(), new PersonRepositoryImpl());
        serverController = new ServerController(personService, new ValidationServiceImpl());
    }

    @Test
    public void testAddToRepository() {
        PersonRequestModel personRequestModel1 = new PersonRequestModel("aaa", "bbb", 5);
        PersonRequestModel personRequestModel2 = new PersonRequestModel("zzz", "xxx", 8);
        PersonRequestModel personRequestModel3 = new PersonRequestModel("ttt", "qqq", 13);

        Assert.assertTrue(serverController.addPerson(personRequestModel1).getStatusCode().equals(HttpStatus.CREATED));
        Assert.assertTrue(serverController.addPerson(personRequestModel2).getStatusCode().equals(HttpStatus.CREATED));
        Assert.assertTrue(serverController.addPerson(personRequestModel3).getStatusCode().equals(HttpStatus.CREATED));

        ResponseEntity<Person> responseEntity1 = (ResponseEntity<Person>) serverController.getPersons("", "", 0);
        ResponseEntity<Person> responseEntity2 = (ResponseEntity<Person>) serverController.getPersons("sdfsdf", "", 0);

        Assert.assertTrue(responseEntity1.getBody().getFirstName().equals("aaa"));
        Assert.assertTrue(responseEntity2.getStatusCode().equals(HttpStatus.NOT_FOUND));
    }
}