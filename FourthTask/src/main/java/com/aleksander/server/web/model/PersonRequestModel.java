package com.aleksander.server.web.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PersonRequestModel {

    private final String firstName;
    private final String lastName;
    private final int age;

    @JsonCreator
    public PersonRequestModel(@JsonProperty("firstname") final String firstName,
                              @JsonProperty("lastname") final String lastName,
                              @JsonProperty("age") final int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isPresentFirstName() {
        return firstName != null;
    }

    public boolean isPresentLastName() {
        return firstName != null;
    }

    public boolean isPresentAge() {
        return age != 0;
    }
}
