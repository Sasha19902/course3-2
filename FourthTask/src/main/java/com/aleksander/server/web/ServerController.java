package com.aleksander.server.web;

import com.aleksander.server.core.model.Person;
import com.aleksander.server.core.services.PersonService;
import com.aleksander.server.core.services.ValidationService;
import com.aleksander.server.web.model.PersonRequestModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Controller
@RequestMapping("/example")
public class ServerController {


    private final ValidationService validationService;
    private final PersonService personService;

    public ServerController(final PersonService personService,
                            final ValidationService validationService) {
        this.validationService = validationService;
        this.personService = personService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getPersons(
            @RequestParam(name = "firstname", defaultValue = "") final String firstName,
            @RequestParam(name = "lastname", defaultValue = "") final String lastName,
            @RequestParam(name = "age", defaultValue = "0") final int age
    ) {
        if(validationService.validName(firstName) &&
           validationService.validName(lastName) &&
           validationService.validAge(age)) {
                Person person = personService.getPersonFiltered(firstName, lastName, age);

                if(person != null) {
                    return ResponseEntity.ok(person);
                } else {

                    return ResponseEntity.notFound().build();
                }
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> addPerson(@RequestBody final PersonRequestModel personRequestModel) {
        if (validationService.validPersonModelRequest(personRequestModel)) {
            Person created = personService.createAndPut(personRequestModel.getFirstName(),
                    personRequestModel.getLastName(),
                    personRequestModel.getAge());

            URI location = UriComponentsBuilder.fromPath("/example/")
                    .path(created.getId())
                    .build()
                    .toUri();
            return ResponseEntity.created(location).build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }
}
