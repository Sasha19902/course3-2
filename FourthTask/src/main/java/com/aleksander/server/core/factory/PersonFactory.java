package com.aleksander.server.core.factory;

import com.aleksander.server.core.model.Person;

import java.util.UUID;

public class PersonFactory {

    public Person create(String firstName, String lastName, int age) {
        return new Person(UUID.randomUUID().toString(), firstName, lastName, age);
    }
}
