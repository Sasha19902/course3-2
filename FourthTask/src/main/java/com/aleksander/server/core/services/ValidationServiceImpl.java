package com.aleksander.server.core.services;

import com.aleksander.server.web.model.PersonRequestModel;
import org.springframework.stereotype.Service;

@Service
public class ValidationServiceImpl implements ValidationService {

    @Override
    public boolean validName(String name) {
        return name != null;
    }

    @Override
    public boolean validAge(int age) {
        return age >= 0;
    }

    @Override
    public boolean validPersonModelRequest(PersonRequestModel personRequestModel) {
        return personRequestModel.isPresentAge() &&
               personRequestModel.isPresentFirstName() &&
               personRequestModel.isPresentLastName();
    }
}
