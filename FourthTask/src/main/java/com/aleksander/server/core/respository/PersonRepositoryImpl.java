package com.aleksander.server.core.respository;

import com.aleksander.server.core.model.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonRepositoryImpl implements PersonRepository {

    private List<Person> persons;

    public PersonRepositoryImpl() {
        this.persons = new ArrayList<>();
    }

    @Override
    public Person getPersonFiltered(
            final String firstName,
            final String lastName,
            final int age) {

        List<Person> result = new ArrayList<>();

        for (Person p: persons) {
            if(!firstName.isEmpty() && !p.getFirstName().equals(firstName)) {
                continue;
            }
            if(!lastName.isEmpty() && !p.getLastName().equals(lastName)) {
                continue;
            }
            if(age != 0 && p.getAge() != age) {
                continue;
            }
            return p;
        }
        return null;
    }

    @Override
    public void putPerson(Person person) {
        persons.add(person);
    }
}
