package com.aleksander.server.core.services;

import com.aleksander.server.web.model.PersonRequestModel;

public interface ValidationService {
    boolean validName(String name);
    boolean validAge(int age);
    boolean validPersonModelRequest(PersonRequestModel personRequestModel);
}
