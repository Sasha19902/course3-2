package com.aleksander.server.core.respository;

import com.aleksander.server.core.model.Person;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface PersonRepository {
    Person getPersonFiltered(String firstName, String lastName, int age);
    void putPerson(Person person);
}
