package com.aleksander.server.core.services;

import com.aleksander.server.core.factory.PersonFactory;
import com.aleksander.server.core.model.Person;
import com.aleksander.server.core.respository.PersonRepository;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl implements PersonService {

    private PersonFactory personFactory;
    private PersonRepository personRepository;

    public PersonServiceImpl(PersonFactory personFactory, PersonRepository personRepository) {
        this.personFactory = personFactory;
        this.personRepository = personRepository;
    }

    @Override
    public Person createAndPut(String firstName, String lastName, int age) {
        Person person = personFactory.create(firstName, lastName, age);
        personRepository.putPerson(person);
        return person;
    }

    @Override
    public Person getPersonFiltered(String firstName, String lastName, int age) {
        return personRepository.getPersonFiltered(firstName, lastName, age);
    }
}
