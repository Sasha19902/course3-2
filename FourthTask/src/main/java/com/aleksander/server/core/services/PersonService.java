package com.aleksander.server.core.services;

import com.aleksander.server.core.model.Person;
import com.aleksander.server.web.model.PersonRequestModel;

import java.util.List;

public interface PersonService {

    Person createAndPut(String firstName, String lastName, int age);
    Person getPersonFiltered(String firstName, String lastName, int age);
}
