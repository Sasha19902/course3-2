package com.aleksander.server.config;

import com.aleksander.server.core.factory.PersonFactory;
import com.aleksander.server.core.respository.PersonRepository;
import com.aleksander.server.core.respository.PersonRepositoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigServices {
    @Bean
    public PersonFactory personService() {
        return new PersonFactory();
    }

    @Bean
    public PersonRepository personRepository() {
        return new PersonRepositoryImpl();
    }
}
