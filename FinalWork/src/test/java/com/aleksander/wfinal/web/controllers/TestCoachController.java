package com.aleksander.wfinal.web.controllers;

import com.aleksander.wfinal.core.model.coach.Coach;
import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.core.services.coach.CoachService;
import com.aleksander.wfinal.core.services.common.CommonService;
import com.aleksander.wfinal.core.services.player.PlayerService;
import com.aleksander.wfinal.core.services.team.TeamService;
import com.aleksander.wfinal.core.services.validation.SimpleValidationService;
import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CoachController.class)
public class TestCoachController {
    @TestConfiguration
    static class TestBeans {
        @Bean
        public TeamService teamService() {
            return mock(TeamService.class);
        }

        @Bean
        public PlayerService playerService() {
            return mock(PlayerService.class);
        }

        @Bean
        public CommonService commonService() {
            return mock(CommonService.class);
        }

        @Bean
        public CoachService coachService() { return mock(CoachService.class); }

        @Bean
        public SimpleValidationService simpleValidationService() {
            return mock(SimpleValidationService.class);
        }
    }

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    public PlayerService playerService;

    @Autowired
    public TeamService teamService;

    @Autowired
    public CommonService commonService;

    @Autowired
    public CoachService coachService;

    @Autowired
    public SimpleValidationService simpleValidationService;

    @Test
    public void testAddCoachTeam() throws Exception {
        Team team = mock(Team.class);
        Coach coach = mock(Coach.class);
        when(coachService.getById(anyLong())).thenReturn(coach);
        when(teamService.getById(anyLong())).thenReturn(team);
        when(coach.getId()).thenReturn(1L);
        when(team.getCoach()).thenReturn(null);
        doNothing().when(coachService).addTeam(1L, team);
        mockMvc.perform(put("/coach/{id}/coach_team/", 1)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content("1"))
                .andExpect(status().isOk());
        verify(coachService, times(1)).addTeam(1L, team);
    }

    @Test
    public void testRemoveCoachTeam() throws Exception {
        Team team = mock(Team.class);
        Coach coach = mock(Coach.class);
        Coach another = mock(Coach.class);
        when(coachService.getById(anyLong())).thenReturn(coach);
        when(teamService.getById(anyLong())).thenReturn(team);
        when(coach.getId()).thenReturn(1L);
        when(another.getId()).thenReturn(1L);
        when(team.getCoach()).thenReturn(another);
        doNothing().when(coachService).removeTeam(team);
        mockMvc.perform(delete("/coach/{id}/coach_team/", 1)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content("1"))
                .andExpect(status().isAccepted());
        verify(coachService, times(1)).removeTeam(team);
    }
}
