package com.aleksander.wfinal.web.controllers;

import com.aleksander.wfinal.core.model.country.Country;
import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.core.services.common.CommonService;
import com.aleksander.wfinal.core.services.player.PlayerService;
import com.aleksander.wfinal.core.services.team.TeamService;
import com.aleksander.wfinal.core.services.validation.SimpleValidationService;
import com.aleksander.wfinal.web.dto.request.CreatePlayerRequest;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(PlayerController.class)
public class TestPlayerController {
    @TestConfiguration
    static class TestBeans {
        @Bean
        public TeamService teamService() {
            return mock(TeamService.class);
        }

        @Bean
        public PlayerService playerService() {
            return mock(PlayerService.class);
        }

        @Bean
        public CommonService commonService() {
            return mock(CommonService.class);
        }

        @Bean
        public SimpleValidationService simpleValidationService() {
            return mock(SimpleValidationService.class);
        }
    }

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    public PlayerService playerService;

    @Autowired
    public TeamService teamService;

    @Autowired
    public CommonService commonService;

    @Autowired
    public SimpleValidationService simpleValidationService;

    @Test
    public void testValidGetRequest() throws Exception {
        Player player = new Player();
        when(playerService.getById(anyLong())).thenReturn(player);
        mockMvc.perform(get("/player/{id}", 1).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(0));
    }

    @Test
    public void testNotFoundGetRequest() throws Exception {
        when(playerService.getById(anyLong())).thenReturn(null);
        mockMvc.perform(get("/player/{id}", anyLong()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testValidPostRequest() throws Exception {
        when(commonService.getCountryById(anyLong())).thenReturn(mock(Country.class));
        Gson gson = new Gson();
        CreatePlayerRequest createPlayerModel = new CreatePlayerRequest("Вася", "aaaa", 2L);
        mockMvc.perform(post("/player/")
                .content(gson.toJson(createPlayerModel))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testNotValidPostRequest() throws Exception {

        Gson gson = new Gson();
        CreatePlayerRequest createPlayerModel = new CreatePlayerRequest("Вася", "", 2L);
        mockMvc.perform(post("/player/")
                .content(gson.toJson(createPlayerModel))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void testFoundedDelete() throws Exception {
        Player player = mock(Player.class);
        when(playerService.getById(anyLong())).thenReturn(player);
        doNothing().when(playerService).deletePlayer(anyLong());

        mockMvc.perform(delete("/player/{id}", 1))
                .andExpect(status().isAccepted());



    }

    @Test
    public void testNotFoundedDelete() throws Exception {
        mockMvc.perform(delete("/player/{id}", 1))
                .andExpect(status().isNotFound());
    }
}
