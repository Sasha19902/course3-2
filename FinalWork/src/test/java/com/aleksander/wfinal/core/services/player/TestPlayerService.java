package com.aleksander.wfinal.core.services.player;

import com.aleksander.wfinal.core.database.dao.interfaces.PlayerDao;
import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.core.services.SpecialDataReturn;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@RunWith(SpringRunner.class)
public class TestPlayerService {
    @TestConfiguration
    static class PlayerServiceBeanConfiguration {
        @Bean
        @Primary
        public PlayerDao playerDao() {
            return mock(PlayerDao.class);
        }

        @Bean
        @Primary
        public PlayerService playerService(@Autowired PlayerDao playerDao) {
            return new PlayerServiceImpl(playerDao);
        }
    }

    @Autowired
    private PlayerService playerService;

    @Autowired
    private PlayerDao playerDao;

    @Test
    public void testGetAll() {
        Collection<Player> specialDataReturn = Mockito.mock(Collection.class);
        Mockito.when(playerDao.getAll(Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(specialDataReturn);
        SpecialDataReturn<Player> returnValue = playerService.getAll("", 1,1);
        Assert.assertEquals(specialDataReturn, returnValue.getData());
    }

    @Test
    public void testInsertPlayer() {
        Player player = mock(Player.class);
        doNothing().when(playerDao).insert(player);
        playerService.addPlayer(player);

        verify(playerDao, times(1)).insert(player);
    }

    @Test
    public void testGetById() {
        Player player = mock(Player.class);
        Mockito.when(playerDao.get(anyLong())).thenReturn(player);
        Player returnValue = playerService.getById(1);
        Assert.assertEquals(player, returnValue);
    }

    @Test
    public void testUpdatePlayer() {
        Player player = mock(Player.class);
        doNothing().when(playerDao).update(player);
        playerService.updatePlayer(player);
        verify(playerDao, times(1)).update(player);
    }

    @Test
    public void testDeleteAll() {
        doNothing().when(playerDao).deleteAll();
        playerService.deleteAll();
        verify(playerDao, times(1)).deleteAll();
    }
}
