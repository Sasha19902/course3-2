package com.aleksander.wfinal.core.services.match;

import com.aleksander.wfinal.core.database.dao.interfaces.MatchDao;
import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.core.model.match.MatchPlayer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class TestMatchService {
    @TestConfiguration
    static class MatchServiceBeanConfiguration {
        @Bean
        @Primary
        public MatchDao commonDao() {
            return mock(MatchDao.class);
        }

        @Bean
        @Primary
        public MatchPlayer matchPlayer() {
            return mock(MatchPlayer.class);
        }

        @Bean
        @Primary
        public MatchService matchService(@Autowired MatchDao matchDao,
                                         @Autowired MatchPlayer matchPlayer) {
            return new MatchServiceImpl(matchDao, matchPlayer);
        }
    }

    @Autowired
    private MatchDao matchDao;

    @Autowired
    private MatchService matchService;

    @Autowired
    private MatchPlayer matchPlayer;

    @Test
    public void testGetMatchById() {
        Match match = mock(Match.class);
        Mockito.when(matchDao.get(anyLong())).thenReturn(match);
        Match returnValue = matchService.getById(1);
        Assert.assertEquals(match, returnValue);
    }

    @Test
    public void testInsertMatch() {
        Match match = mock(Match.class);
        doNothing().when(matchDao).insert(match);
        matchService.addMatch(match);

        verify(matchDao, times(1)).insert(match);
    }

    @Test
    public void testPlayMatch() {
        Match match = mock(Match.class);
        doNothing().when(matchDao).playMatch(match);
        doNothing().when(matchPlayer).playMatch(match);
        matchService.playMatch(match);

        verify(matchDao, times(1)).playMatch(match);
        verify(matchPlayer, times(1)).playMatch(match);
    }

    @Test
    public void testDeleteAll() {
        doNothing().when(matchDao).deleteAll();
        matchService.deleteAll();
        verify(matchDao, times(1)).deleteAll();
    }
}
