package com.aleksander.wfinal.core.services.team;

import com.aleksander.wfinal.core.database.dao.interfaces.TeamDao;
import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.core.services.SpecialDataReturn;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@RunWith(SpringRunner.class)
public class TestTeamService {
    @TestConfiguration
    static class TeamServiceBeanConfiguration {
        @Bean
        @Primary
        public TeamDao teamDao() {
            return mock(TeamDao.class);
        }

        @Bean
        @Primary
        public TeamService teamService(@Autowired TeamDao teamDao) {
            return new TeamServiceImpl(teamDao);
        }
    }

    @Autowired
    private TeamService teamService;

    @Autowired
    private TeamDao teamDao;

    @Test
    public void testGetAll() {
        Collection<Team> specialDataReturn = Mockito.mock(Collection.class);
        Mockito.when(teamDao.getAll(Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(specialDataReturn);
        SpecialDataReturn<Team> returnValue = teamService.getAll("", 1,1);
        Assert.assertEquals(specialDataReturn, returnValue.getData());
    }

    @Test
    public void testInsertTeam() {
        Team team = mock(Team.class);
        doNothing().when(teamDao).insert(team);
        teamService.addTeam(team);

        verify(teamDao, times(1)).insert(team);
    }

    @Test
    public void testGetById() {
        Team team = mock(Team.class);
        Mockito.when(teamDao.get(anyLong())).thenReturn(team);
        Team returnValue = teamService.getById(1L);
        Assert.assertEquals(team, returnValue);
    }

    @Test
    public void testUpdatePlayer() {
        Team team = mock(Team.class);
        doNothing().when(teamDao).update(team);
        teamService.updateTeam(team);
        verify(teamDao, times(1)).update(team);
    }

    @Test
    public void testDeleteById() {
        doNothing().when(teamDao).delete(anyLong());
        teamService.deleteTeam(1L);
        verify(teamDao, times(1)).delete(1L);
    }

    @Test
    public void testDeleteAll() {
        doNothing().when(teamDao).deleteAll();
        teamService.deleteAll();
        verify(teamDao, times(1)).deleteAll();
    }
}
