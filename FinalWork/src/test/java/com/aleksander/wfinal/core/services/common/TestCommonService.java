package com.aleksander.wfinal.core.services.common;

import com.aleksander.wfinal.core.database.dao.interfaces.CommonDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class TestCommonService {


    @TestConfiguration
    static class CommonServiceBeanConfiguration {
        @Bean
        @Primary
        public CommonDao commonDao() {
            return Mockito.mock(CommonDao.class);
        }

        @Bean
        @Primary
        public CommonService commonService(@Autowired CommonDao commonDao) {
            return new CommonServiceImpl(commonDao);
        }
    }

    @Autowired
    private CommonDao commonDao;

    @Autowired
    private CommonService commonService;

    @Test
    public void positionsTest() {
        commonService.positions();
        Mockito.verify(commonDao).positions();
    }

    @Test
    public void getByIdTest() {
        commonService.getPositionById(1);
        Mockito.verify(commonDao).getPositionById(1);
    }
}
