package com.aleksander.wfinal.web.client.interfaces;

import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.web.dto.request.CreateTeamModel;
import com.aleksander.wfinal.web.dto.request.EditTeamRequest;
import com.aleksander.wfinal.web.dto.response.ManyDto;
import com.aleksander.wfinal.web.dto.response.match.SingleMatchDto;

import java.net.URI;

public interface TeamClient {
    URI addTeam(CreateTeamModel createTeamModel);
    Team getById(long id);
    void updateTeam(Long id, EditTeamRequest editTeamRequest);
    void addPlayerToTeam(long teamId, long playerId);
    ManyDto<SingleMatchDto> getAll(Long teamId);
}
