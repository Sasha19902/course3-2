package com.aleksander.wfinal.web.dto.response.team;

import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.web.dto.response.meta.TeamMetaData;
import com.aleksander.wfinal.web.dto.response.player.ManyPlayerDto;

public class ManyTeamDto {
    private long id;
    private String name;
    private TeamMetaData teamMetaData;

    public ManyTeamDto()  { }

    public ManyTeamDto(Team team) {
        this.id = team.getId();
        this.name = team.getName();
        this.teamMetaData = new TeamMetaData(team);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TeamMetaData getTeamMetaData() {
        return teamMetaData;
    }

    public void setTeamMetaData(TeamMetaData teamMetaData) {
        this.teamMetaData = teamMetaData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
