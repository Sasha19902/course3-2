package com.aleksander.wfinal.web.controllers;

import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.core.services.match.MatchService;
import com.aleksander.wfinal.core.services.team.TeamService;
import com.aleksander.wfinal.web.dto.request.CreateMatchRequest;
import com.aleksander.wfinal.web.dto.response.match.SingleMatchDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@Controller
@RequestMapping("match/")
public class MatchController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerController.class);

    private MatchService matchService;
    private TeamService teamService;

    public MatchController(MatchService matchService,
                           TeamService teamService) {
        this.matchService = matchService;
        this.teamService = teamService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getMatch(@PathVariable("id") final String path) {
        Match match = matchService.getById(Long.parseLong(path));
        if(match != null) {
            return ResponseEntity.ok(new SingleMatchDto(match));
        }
        return ResponseEntity.notFound().build();
    }



    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> addMatch(@Valid @RequestBody CreateMatchRequest createMatchRequest,
                                      BindingResult bindingResult,
                                      Model model) {

        Team leftTeam = teamService.getById(createMatchRequest.getFirstTeamId());
        Team rightTeam = teamService.getById(createMatchRequest.getSecondTeamId());
        if(leftTeam != null && rightTeam !=null) {
            if (bindingResult.hasErrors()) {
                model.addAllAttributes(ControllerUtils.getErrors(bindingResult));
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            } else {
                Match match = new Match(0, leftTeam, rightTeam);
                matchService.addMatch(match);
                URI location = UriComponentsBuilder.fromPath("/match/")
                        .path(Long.toString(match.getId()))
                        .build()
                        .toUri();
                LOGGER.info(String.format("ADD to repository match with id: %d", match.getId()));
                return ResponseEntity.created(location).build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> playMatch(@PathVariable("id") String path) {
        Match match = matchService.getById(Long.parseLong(path));
        if(match == null) {
            return ResponseEntity.notFound().build();
        } else {
            if(match.getGameStatistic() == null) {
                matchService.playMatch(match);
            }
            return ResponseEntity.ok().build();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> deleteMatch(@PathVariable("id") final String path) {
        Match match = matchService.getById(Long.parseLong(path));
        if (match != null) {
            matchService.deleteMatch(Long.parseLong(path));
            return ResponseEntity.accepted().build();
        }
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> deleteAll() {
        matchService.deleteAll();
        return ResponseEntity.ok().build();
    }
}
