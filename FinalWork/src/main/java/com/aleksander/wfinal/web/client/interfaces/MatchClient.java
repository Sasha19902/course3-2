package com.aleksander.wfinal.web.client.interfaces;

import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.web.dto.request.CreateMatchRequest;
import com.aleksander.wfinal.web.dto.response.match.SingleMatchDto;

import java.net.URI;

public interface MatchClient {
    URI addMatch(CreateMatchRequest createMatchRequest);

    SingleMatchDto getById(long id);

    void playMatch(long id);
}
