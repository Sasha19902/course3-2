package com.aleksander.wfinal.web.dto.response.coach;

import com.aleksander.wfinal.core.model.coach.Coach;
import com.aleksander.wfinal.core.model.country.Country;
import com.aleksander.wfinal.web.dto.response.meta.TeamMetaData;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class SingleCoachDto {
    private long id;
    private String name;
    private Country country;
    private List<TeamMetaData> teamMetaData;

    public SingleCoachDto() { }

    public SingleCoachDto(Coach coach) {
        this.id = coach.getId();
        this.name = coach.getFirstName() + " " + coach.getSecondName();
        this.country = coach.getCountry();
        if(coach.getTeams() != null && !coach.getTeams().isEmpty()) {
            teamMetaData = coach.getTeams().stream().map(TeamMetaData::new).collect(Collectors.toList());
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<TeamMetaData> getTeamMetaData() {
        return teamMetaData;
    }

    public void setTeamMetaData(List<TeamMetaData> teamMetaData) {
        this.teamMetaData = teamMetaData;
    }
}
