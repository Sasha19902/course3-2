package com.aleksander.wfinal.web.dto.request;

import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.core.services.common.CommonService;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;

public class CreatePlayerRequest implements Serializable {
    @NotBlank(message = "first name is required field")
    private String firstName;
    @NotBlank(message = "second name is required field")
    private String secondName;
    @NotNull(message = "country is required field")
    @Positive(message = "country is required field")
    private Long countryId;

    public CreatePlayerRequest() { }

    public CreatePlayerRequest(String firstName,
                               String secondName,
                               Long countryId) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.countryId = countryId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Player getPlayerFromRequest(CommonService commonService) {
        return new Player(0, firstName, secondName, commonService.getCountryById(countryId));
    }
}
