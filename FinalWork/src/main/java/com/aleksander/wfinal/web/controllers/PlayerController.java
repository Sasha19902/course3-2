package com.aleksander.wfinal.web.controllers;


import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.core.services.SpecialDataReturn;
import com.aleksander.wfinal.core.services.common.CommonService;
import com.aleksander.wfinal.core.services.player.PlayerService;
import com.aleksander.wfinal.core.services.team.TeamService;
import com.aleksander.wfinal.core.services.validation.SimpleValidationService;
import com.aleksander.wfinal.web.dto.request.CreatePlayerRequest;
import com.aleksander.wfinal.web.dto.request.EditPlayerRequest;
import com.aleksander.wfinal.web.dto.response.ManyDto;
import com.aleksander.wfinal.web.dto.response.meta.PagingMeta;
import com.aleksander.wfinal.web.dto.response.player.ManyPlayerDto;
import com.aleksander.wfinal.web.dto.response.player.SinglePlayerDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.stream.Collectors;

@Controller
@RequestMapping("player/")
public class PlayerController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerController.class);

    private PlayerService playerService;
    private TeamService teamService;
    private CommonService commonService;
    private SimpleValidationService simpleValidationService;

    public PlayerController(TeamService teamService,
                            PlayerService service,
                            CommonService commonService,
                            SimpleValidationService simpleValidationService) {
        this.playerService = service;
        this.teamService = teamService;
        this.commonService = commonService;
        this.simpleValidationService = simpleValidationService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getAllPlayers(
            @RequestParam(name = "order", defaultValue = "asc") final String order,
            @RequestParam(name = "page", defaultValue = "1") final int page,
            @RequestParam(name = "size", defaultValue = "25") final int size) {

        if (simpleValidationService.validRequestPage(page) &&
                simpleValidationService.validSizePage(size) &&
                simpleValidationService.validTypeOrder(order)) {
            SpecialDataReturn<Player> players = playerService.getAll(order, page, size);
            PagingMeta pagingMeta = new PagingMeta("player",
                    players.getAllCount(), page, size, order);
            ManyDto<ManyPlayerDto> playerManyDto = new ManyDto<>(pagingMeta,
                    players.getData().stream().map(ManyPlayerDto::new).collect(Collectors.toList()));
            LOGGER.info("PlayerController: get all players");
            return ResponseEntity.ok(playerManyDto);
        }
        return ResponseEntity.badRequest().build();
    }


    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> addPlayer(@Valid @RequestBody CreatePlayerRequest createPlayerRequest,
                                       BindingResult bindingResult,
                                       Model model) {
        if (bindingResult.hasErrors()) {
            model.addAllAttributes(ControllerUtils.getErrors(bindingResult));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } else {
            Player player = createPlayerRequest.getPlayerFromRequest(commonService);
            playerService.addPlayer(player);
            URI location = UriComponentsBuilder.fromPath("/player/")
                    .path(Long.toString(player.getId()))
                    .build()
                    .toUri();
            LOGGER.info(String.format("PlayerController: add to database player with id: %d", player.getId()));
            return ResponseEntity.created(location).build();
        }
    }

    //done
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getPlayer(@PathVariable("id") final String path) {
        Player player = playerService.getById(Long.parseLong(path));

        if (player != null) {
            return ResponseEntity.ok(new SinglePlayerDto(player));
        }
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> deletePlayer(@PathVariable("id") final String path) {
        Player player = playerService.getById(Integer.parseInt(path));
        if (player != null) {
            playerService.deletePlayer(Long.parseLong(path));
            return ResponseEntity.accepted().build();
        }
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> deleteAll() {
        playerService.deleteAll();
        return ResponseEntity.accepted().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> updatePlayer(@PathVariable("id") final String path,
                                          @Valid @RequestBody EditPlayerRequest editPlayerRequest,
                                          BindingResult bindingResult,
                                          Model model) {
        Player player = playerService.getById(Integer.parseInt(path));
        if (player == null) {
            return ResponseEntity.notFound().build();
        } else {
            if (bindingResult.hasErrors()) {
                model.addAllAttributes(ControllerUtils.getErrors(bindingResult));
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            } else {
                playerService.updatePlayer(editPlayerRequest.editedPlayer(player, commonService, teamService));
                return ResponseEntity.ok().build();
            }
        }
    }

    @ExceptionHandler(NumberFormatException.class)
    @ResponseBody
    public ResponseEntity<?> casting() {
        return ResponseEntity.badRequest().build();
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<?> handler() {
        return ResponseEntity.badRequest().build();
    }
}