package com.aleksander.wfinal.web.dto.response.meta;


import com.aleksander.wfinal.core.model.player.Player;

import java.io.Serializable;

public class PlayerMetaData implements Serializable {
    private String path;
    private String name;

    public PlayerMetaData() { }

    public PlayerMetaData(Player player) {
        name = player.getFirstName() + " " + player.getSecondName();
        path = "/player/" + player.getId();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
