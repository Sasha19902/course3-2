package com.aleksander.wfinal.web.dto.response.meta;

public class PagingMeta {

    private static final String CONTENT_TEMPLATE = "/%s/?order=%s&page=%d&size=%d";

    private long total;
    private int page;
    private int size;
    private String next;
    private String prev;
    private String first;
    private String last;

    public PagingMeta() { }

    public PagingMeta(final String type,
                      final long total,
                      final int page,
                      final int size,
                      final String order
    ) {
        this.total = total;
        this.page = page;
        this.size = size;

        int lastPage = (int) Math.round(Math.ceil((double) total / size));
        lastPage = lastPage == 0 ? 1 : lastPage;

        next = String.format(CONTENT_TEMPLATE, type, order, page < lastPage ? page + 1 : lastPage, size);
        prev = String.format(CONTENT_TEMPLATE, type, order, page > 1 && page < lastPage ? page - 1 : 1, size);
        first = String.format(CONTENT_TEMPLATE, type, order, 1, size);
        last = String.format(CONTENT_TEMPLATE, type, order, lastPage, size);
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(final long total) {
        this.total = total;
    }

    public int getPage() {
        return page;
    }

    public void setPage(final int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(final int size) {
        this.size = size;
    }

    public String getNext() {
        return next;
    }

    public void setNext(final String next) {
        this.next = next;
    }

    public String getPrev() {
        return prev;
    }

    public void setPrev(final String prev) {
        this.prev = prev;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(final String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(final String last) {
        this.last = last;
    }
}