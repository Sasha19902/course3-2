package com.aleksander.wfinal.web.dto.request;

import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.core.services.team.TeamService;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class CreateMatchRequest {
    @NotNull(message = "first team is required field")
    @Positive
    private Long firstTeamId;
    @NotNull(message = "second team is required field")
    @Positive
    private Long secondTeamId;

    public CreateMatchRequest(Long firstTeamId,
                              Long secondTeamId) {
        this.firstTeamId = firstTeamId;
        this.secondTeamId = secondTeamId;
    }

    public Long getFirstTeamId() {
        return firstTeamId;
    }

    public void setFirstTeamId(Long firstTeamId) {
        this.firstTeamId = firstTeamId;
    }

    public Long getSecondTeamId() {
        return secondTeamId;
    }

    public void setSecondTeamId(Long secondTeamId) {
        this.secondTeamId = secondTeamId;
    }

    public Match getPlayerFromRequest(TeamService teamService) {
        return new Match(0, teamService.getById(firstTeamId), teamService.getById(secondTeamId));
    }
}
