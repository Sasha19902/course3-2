package com.aleksander.wfinal.web.controllers;
import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.core.services.SpecialDataReturn;
import com.aleksander.wfinal.core.services.coach.CoachService;
import com.aleksander.wfinal.core.services.common.CommonService;
import com.aleksander.wfinal.core.services.player.PlayerService;
import com.aleksander.wfinal.core.services.team.TeamService;
import com.aleksander.wfinal.core.services.validation.SimpleValidationService;
import com.aleksander.wfinal.web.dto.request.CreateTeamModel;
import com.aleksander.wfinal.web.dto.request.EditTeamRequest;
import com.aleksander.wfinal.web.dto.response.ManyDto;
import com.aleksander.wfinal.web.dto.response.match.SingleMatchDto;
import com.aleksander.wfinal.web.dto.response.meta.PagingMeta;
import com.aleksander.wfinal.web.dto.response.team.ManyTeamDto;
import com.aleksander.wfinal.web.dto.response.team.SingleTeamDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collection;
import java.util.stream.Collectors;

@Controller
@RequestMapping("team/")
public class TeamController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeamController.class);
    private TeamService teamService;
    private PlayerService playerService;
    private CoachService coachService;
    private CommonService commonService;
    private SimpleValidationService simpleValidationService;

    public TeamController(TeamService teamService,
                          PlayerService playerService,
                          SimpleValidationService simpleValidationService,
                          CommonService commonService,
                          CoachService coachService) {
        this.teamService = teamService;
        this.playerService = playerService;
        this.simpleValidationService = simpleValidationService;
        this.commonService = commonService;
        this.coachService = coachService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getAllTeams(
            @RequestParam(name = "order", defaultValue = "asc") final String order,
            @RequestParam(name = "page", defaultValue = "1") final int page,
            @RequestParam(name = "size", defaultValue = "25") final int size) {

        if (simpleValidationService.validRequestPage(page) &&
                simpleValidationService.validSizePage(size) &&
                simpleValidationService.validTypeOrder(order)) {
            SpecialDataReturn<Team> teams = teamService.getAll(order, page, size);
            PagingMeta pagingMeta = new PagingMeta("team",
                    teams.getAllCount(), page, size, order);
            ManyDto<ManyTeamDto> teamManyDto = new ManyDto<>(pagingMeta,
                    teams.getData().stream().map(ManyTeamDto::new).collect(Collectors.toList()));
            return ResponseEntity.ok(teamManyDto);
        }
        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> addTeam(@Valid @RequestBody CreateTeamModel createTeamModel,
                                     BindingResult bindingResult,
                                     Model model) {

        if(bindingResult.hasErrors()) {
            model.addAllAttributes(ControllerUtils.getErrors(bindingResult));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } else {
            Team team = createTeamModel.getTeamFromRequest(commonService);
            teamService.addTeam(team);
            URI location = UriComponentsBuilder.fromPath("/team/")
                    .path(Long.toString(team.getId()))
                    .build()
                    .toUri();
            LOGGER.info(String.format("TeamController: add to database team with id: %d", team.getId()));
            return ResponseEntity.created(location).build();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getTeam(@PathVariable("id") final String path) {
        Team Team = teamService.getById(Long.parseLong(path));
        if(Team != null) {
            return ResponseEntity.ok(new SingleTeamDto(Team));
        }
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> deleteTeam(@PathVariable("id") final String path) {
        Team team = teamService.getById(Long.parseLong(path));
        if(team != null) {
            teamService.deleteTeam(Long.parseLong(path));
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> deleteAll() {
        teamService.deleteAll();
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> updateTeam(@PathVariable("id") final String path,
                                          @Valid @RequestBody EditTeamRequest editTeamRequest,
                                          BindingResult bindingResult,
                                          Model model) {
        Team team = teamService.getById(Long.parseLong(path));
        if(team == null) {
            return ResponseEntity.notFound().build();
        } else {
            if(bindingResult.hasErrors()) {
                model.addAllAttributes(ControllerUtils.getErrors(bindingResult));
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            } else {
                teamService.updateTeam(editTeamRequest.editedTeam(team, commonService, coachService));
                return ResponseEntity.ok().build();
            }
        }
    }

    @RequestMapping(value = "/{id}/players/", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> addPlayerToTeam(@PathVariable("id") String path, @RequestBody Long playerId) {
        long teamId = Long.parseLong(path);
        Team team = teamService.getById(teamId);
        Player player = playerService.getById(playerId);
        if(team != null && player != null) {
            if(player.getTeam() == null) {
                teamService.addPlayerToTeam(team.getId(), playerId);
            } else {
                if(player.getTeam().getId() != teamId) {
                    return ResponseEntity.badRequest().build();
                }
            }
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/{id}/players/", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> removePlayerFromTeam(@PathVariable("id") String path, @RequestBody Long playerId) {
        long teamId = Long.parseLong(path);
        Team team = teamService.getById(teamId);
        Player player = playerService.getById(playerId);
        if(team != null && player != null) {
            if(player.getTeam() != null) {
                if(player.getTeam().getId() != teamId) {
                    return ResponseEntity.badRequest().build();
                } else {
                    teamService.removePlayerFromTeam(teamId, playerId);
                    return ResponseEntity.ok().build();
                }
            } else {
                return ResponseEntity.badRequest().build();
            }
        }
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/{id}/matches/", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> matchPlayedByTeam(@PathVariable("id") String path,
                                               @RequestParam(name = "order", defaultValue = "asc") final String order,
                                               @RequestParam(name = "page", defaultValue = "1") final int page,
                                               @RequestParam(name = "size", defaultValue = "25") final int size) {
        if (simpleValidationService.validRequestPage(page) &&
                simpleValidationService.validSizePage(size) &&
                simpleValidationService.validTypeOrder(order)) {

            long teamId = Long.parseLong(path);
            Team team = teamService.getById(teamId);
            if (team != null) {
                SpecialDataReturn<Match> matches = teamService.matchesPlayedByTeam(team, order, page, size);
                PagingMeta pagingMeta = new PagingMeta(String.format("/%d/matches/", teamId),
                        matches.getAllCount(), page, size, order);
                ManyDto<SingleMatchDto> teamManyDto = new ManyDto<>(pagingMeta,
                        matches.getData().stream().map(SingleMatchDto::new).collect(Collectors.toList()));
                return ResponseEntity.ok(teamManyDto);
            } else {
                return ResponseEntity.notFound().build();
            }
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @ExceptionHandler(NumberFormatException.class)
    @ResponseBody
    public ResponseEntity<?> casting() {
        return ResponseEntity.notFound().build();
    }
}
