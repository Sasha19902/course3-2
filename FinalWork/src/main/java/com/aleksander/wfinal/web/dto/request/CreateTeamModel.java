package com.aleksander.wfinal.web.dto.request;

import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.core.services.common.CommonService;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.util.Objects;

public class CreateTeamModel {
    @NotBlank(message = "name is required field")
    private String name;
    @Positive(message = "country is required field")
    private Long countryId;

    public CreateTeamModel(String name,
                           Long countryId) {
        this.name = name;
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Team getTeamFromRequest(CommonService commonService) {
        return new Team(0, name, commonService.getCountryById(countryId));
    }
}
