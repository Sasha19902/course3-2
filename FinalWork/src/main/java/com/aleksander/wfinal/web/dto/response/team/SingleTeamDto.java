package com.aleksander.wfinal.web.dto.response.team;

import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.web.dto.response.meta.PlayerMetaData;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class SingleTeamDto {
    private long id;
    private String name;
    private List<PlayerMetaData> players;

    public SingleTeamDto()  { }

    public SingleTeamDto(Team team) {
        this.id = team.getId();
        this.name = team.getName();
        this.players = team.getPlayerList().stream().map(PlayerMetaData::new).collect(toList());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PlayerMetaData> getPlayers() {
        return players;
    }

    public void setPlayers(List<PlayerMetaData> players) {
        this.players = players;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
