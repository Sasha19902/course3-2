package com.aleksander.wfinal.web.dto.response.meta;

import com.aleksander.wfinal.core.model.coach.Coach;

public class CoachMetaData {
    private String path;
    private String name;

    public CoachMetaData() { }

    public CoachMetaData(Coach coach) {
        name = coach.getFirstName() + " " + coach.getSecondName();
        path = "/player/" + coach.getId();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
