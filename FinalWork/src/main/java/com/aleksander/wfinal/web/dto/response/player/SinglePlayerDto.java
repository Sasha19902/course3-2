package com.aleksander.wfinal.web.dto.response.player;

import com.aleksander.wfinal.core.model.country.Country;
import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.core.model.player.Position;
import com.aleksander.wfinal.web.dto.response.meta.TeamMetaData;

public class SinglePlayerDto {
    private long id;
    private String name;
    private Country country;
    private Position position;
    private int playerNumber;
    private TeamMetaData teamMetaData;

    public SinglePlayerDto()  { }

    public SinglePlayerDto(Player player) {
        this.id = player.getId();
        this.country = player.getCountry();
        this.name = player.getFirstName() + " " + player.getSecondName();
        this.playerNumber = player.getPlayerNumber();
        this.position = player.getPosition();
        if(player.getTeam() != null) {
            this.teamMetaData = new TeamMetaData(player.getTeam());
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public int getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    public TeamMetaData getTeamMetaData() {
        return teamMetaData;
    }

    public void setTeamMetaData(TeamMetaData teamMetaData) {
        this.teamMetaData = teamMetaData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
