package com.aleksander.wfinal.web.client.interfaces;

import com.aleksander.wfinal.core.model.coach.Coach;
import com.aleksander.wfinal.web.dto.request.CreateCoachRequest;

import java.net.URI;

public interface CoachClient {
    URI addCoach(CreateCoachRequest createCoachRequest);
    Coach getById(long id);
}
