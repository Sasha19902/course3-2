package com.aleksander.wfinal.web.dto.response.player;

import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.web.dto.response.meta.PlayerMetaData;

import java.io.Serializable;

public class ManyPlayerDto implements Serializable {
    private long id;
    private String name;
    private PlayerMetaData playerMetaData;

    public ManyPlayerDto() { }

    public ManyPlayerDto(Player player) {
        this.id = player.getId();
        this.name = player.getFirstName() + " " + player.getSecondName();
        this.playerMetaData = new PlayerMetaData(player);
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PlayerMetaData getPlayerMetaData() {
        return playerMetaData;
    }

    public void setPlayerMetaData(PlayerMetaData playerMetaData) {
        this.playerMetaData = playerMetaData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
