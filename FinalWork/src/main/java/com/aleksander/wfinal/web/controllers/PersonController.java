package com.aleksander.wfinal.web.controllers;

import com.aleksander.wfinal.core.model.person.Person;
import com.aleksander.wfinal.core.services.common.CommonService;
import com.aleksander.wfinal.core.services.person.PersonService;
import com.aleksander.wfinal.web.dto.request.EditPersonRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("person/")
public class PersonController {
    private PersonService personService;
    private CommonService commonService;

    public PersonController(PersonService personService,
                            CommonService commonService) {
        this.personService = personService;
        this.commonService = commonService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> updatePerson(@PathVariable("id") final String path,
                                          @Valid @RequestBody EditPersonRequest editPersonRequest,
                                          BindingResult bindingResult,
                                          Model model) {
        Person person = personService.getById(Long.parseLong(path));
        if (person == null) {
            return ResponseEntity.notFound().build();
        } else {
            if (bindingResult.hasErrors()) {
                model.addAllAttributes(ControllerUtils.getErrors(bindingResult));
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            } else {
                personService.updatePerson(editPersonRequest.editedPerson(person, commonService));
                return ResponseEntity.ok().build();
            }
        }
    }
}
