package com.aleksander.wfinal.web.client.implementation;

import com.aleksander.wfinal.web.client.interfaces.PlayerClient;
import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.web.dto.request.CreatePlayerRequest;
import com.aleksander.wfinal.web.dto.response.ManyDto;
import com.aleksander.wfinal.web.dto.response.player.ManyPlayerDto;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collection;

public class PlayerClientImpl implements PlayerClient {

    private static final String ROOT_URI = "http://localhost:8080/player/";
    private static final String ROOT_G_ALL_MODIFICATOR = "?order=%s&page=%d&size=%d";
    private RestTemplate restTemplate;

    public PlayerClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public ManyDto<ManyPlayerDto> getAll(String order, int page, int size) {
        ResponseEntity<?> responseEntity = restTemplate.exchange(
                String.format(ROOT_URI+ROOT_G_ALL_MODIFICATOR, order, page, size),
                HttpMethod.GET,null , new ParameterizedTypeReference<ManyDto<ManyPlayerDto>>() {} );
        return (ManyDto<ManyPlayerDto>) responseEntity.getBody();
    }

    @Override
    public URI addPlayer(CreatePlayerRequest createPlayerRequest) {
        URI response = restTemplate.postForLocation(ROOT_URI, createPlayerRequest);
        return response;
    }

    @Override
    public Player getById(long id) {
        ResponseEntity<Player> player = restTemplate.getForEntity(ROOT_URI + "/"+ id, Player.class);
        return player.getBody();
    }

    @Override
    public HttpStatus deletePlayer(long id) {
        return null;
    }

    @Override
    public HttpStatus updatePlayer(Player player) {
        return null;
    }

    @Override
    public HttpStatus deleteAll() {
        return null;
    }
}
