package com.aleksander.wfinal.web.dto.request;

import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.core.services.common.CommonService;
import com.aleksander.wfinal.core.services.common.CommonServiceImpl;
import com.aleksander.wfinal.core.services.team.TeamService;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.*;

public class EditPlayerRequest {
    @Positive(message = "Not negative id")
    private Long teamId;
    @Positive(message = "Not negative id")
    private Long positionId;
    @Positive(message = "Not negative id")
    private Integer playerNumber;


    public EditPlayerRequest(Long teamId,
                             Long positionId,
                             Integer playerNumber) {

        this.teamId = teamId;
        this.positionId = positionId;
        this.playerNumber = playerNumber;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Integer getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(Integer playerNumber) {
        this.playerNumber = playerNumber;
    }

    public Player editedPlayer(Player player, CommonService commonService, TeamService teamService) {
        if(null != positionId) {
            player.setPosition(commonService.getPositionById(positionId));
        }
        if(null != teamId) {
            player.setTeam(teamService.getById(teamId));
        }
        if(null != playerNumber) {
            player.setPlayerNumber(playerNumber);
        }
        return player;
    }
}
