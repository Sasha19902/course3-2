package com.aleksander.wfinal.web.dto.response.coach;

import com.aleksander.wfinal.core.model.coach.Coach;
import com.aleksander.wfinal.web.dto.response.meta.CoachMetaData;

public class ManyCoachDto {
    private long id;
    private String name;
    private CoachMetaData coachMetaData;

    public ManyCoachDto() {}

    public ManyCoachDto(Coach coach) {
        this.id = coach.getId();
        this.name = coach.getFirstName() + " " + coach.getSecondName();
        this.coachMetaData = new CoachMetaData(coach);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CoachMetaData getCoachMetaData() {
        return coachMetaData;
    }

    public void setCoachMetaData(CoachMetaData coachMetaData) {
        this.coachMetaData = coachMetaData;
    }
}
