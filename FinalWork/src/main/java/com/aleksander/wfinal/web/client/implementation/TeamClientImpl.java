package com.aleksander.wfinal.web.client.implementation;

import com.aleksander.wfinal.web.client.interfaces.TeamClient;
import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.web.dto.request.CreateTeamModel;
import com.aleksander.wfinal.web.dto.request.EditTeamRequest;
import com.aleksander.wfinal.web.dto.response.ManyDto;
import com.aleksander.wfinal.web.dto.response.match.SingleMatchDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

public class TeamClientImpl implements TeamClient {

    private RestTemplate restTemplate;
    private static final String ROOT_URI = "http://localhost:8080/team/";

    public TeamClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public URI addTeam(CreateTeamModel createTeamModel) {
        URI response = restTemplate.postForLocation(ROOT_URI, createTeamModel);
        return response;
    }

    @Override
    public Team getById(long id) {
        return null;
    }

    @Override
    public void updateTeam(Long id, EditTeamRequest editTeamRequest) {
        restTemplate.put(ROOT_URI + id + "/", editTeamRequest);
    }

    @Override
    public void addPlayerToTeam(long teamId, long playerId) {
        restTemplate.put(ROOT_URI + teamId + "/players/", playerId);
    }

    @Override
    public ManyDto<SingleMatchDto> getAll(Long teamId) {
        ResponseEntity<?> responseEntity = restTemplate.exchange(
                ROOT_URI+ teamId +"/matches/",
                HttpMethod.GET,
                null ,
                new ParameterizedTypeReference<ManyDto<SingleMatchDto>>() {} );
        return (ManyDto<SingleMatchDto>) responseEntity.getBody();
    }

}
