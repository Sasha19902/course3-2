package com.aleksander.wfinal.web.dto.response;

import com.aleksander.wfinal.web.dto.response.meta.PagingMeta;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;

public class ManyDto<T> {
    private PagingMeta pagingMeta;
    private Collection<T> jsonSerializableData;

    public ManyDto() {}

    public ManyDto(PagingMeta pagingMeta, Collection<T> jsonSerializableData) {
        this.pagingMeta = pagingMeta;
        this.jsonSerializableData = jsonSerializableData;
    }

    public PagingMeta getPagingMeta() {
        return pagingMeta;
    }

    public void setPagingMeta(PagingMeta pagingMeta) {
        this.pagingMeta = pagingMeta;
    }

    public Collection<T> getJsonSerializableData() {
        return jsonSerializableData;
    }

    public void setJsonSerializableData(Collection<T> jsonSerializableData) {
        this.jsonSerializableData = jsonSerializableData;
    }
}
