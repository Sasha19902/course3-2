package com.aleksander.wfinal.web.dto.request;

import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.core.services.coach.CoachService;
import com.aleksander.wfinal.core.services.common.CommonService;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class EditTeamRequest {
    @Positive(message = "Not negative id")
    private Long coachId;
    @Positive(message = "Not negative id")
    private Long countryId;
    @NotNull(message = "Not empty name")
    private String name;

    public EditTeamRequest(Long coachId,
                           Long countryId,
                           String name) {

        this.coachId = coachId;
        this.countryId = countryId;
        if(name == null) {
            this.name = "";
        } else {
            this.name = name;
        }

    }

    public Long getCoachId() {
        return coachId;
    }

    public void setCoachId(Long coachId) {
        this.coachId = coachId;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Team editedTeam(Team team, CommonService commonService, CoachService coachService) {
        if (null != countryId && countryId != 0) {
            team.setCountry(commonService.getCountryById(countryId));
        }
        if (null != coachId) {
            team.setCoach(coachService.getById(coachId));
        }
        if (!name.isEmpty()) {
            team.setName(name);
        }
        return team;
    }
}
