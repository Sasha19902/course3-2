package com.aleksander.wfinal.web.client.implementation;

import com.aleksander.wfinal.web.client.interfaces.MatchClient;
import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.web.dto.request.CreateMatchRequest;
import com.aleksander.wfinal.web.dto.response.match.SingleMatchDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

public class MatchClientImpl implements MatchClient {

    private static final String ROOT_URI = "http://localhost:8080/match/";
    private RestTemplate restTemplate;

    public MatchClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    @Override
    public URI addMatch(CreateMatchRequest createMatchRequest) {
        URI response = restTemplate.postForLocation(ROOT_URI, createMatchRequest);
        return response;
    }

    @Override
    public SingleMatchDto getById(long id) {
        ResponseEntity<SingleMatchDto> player = restTemplate.getForEntity(ROOT_URI + "/"+ id, SingleMatchDto.class);
        return player.getBody();
    }

    @Override
    public void playMatch(long id) {
        restTemplate.put(ROOT_URI + id, null);
    }
}
