package com.aleksander.wfinal.web.client.interfaces;

import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.web.dto.request.CreatePlayerRequest;
import com.aleksander.wfinal.web.dto.response.ManyDto;
import com.aleksander.wfinal.web.dto.response.player.ManyPlayerDto;
import org.springframework.http.HttpStatus;

import java.net.URI;
import java.util.Collection;

public interface PlayerClient {

    ManyDto<ManyPlayerDto> getAll(String order, int page, int size);

    URI addPlayer(CreatePlayerRequest createPlayerRequest);

    Player getById(long id);

    HttpStatus deletePlayer(long id);

    HttpStatus updatePlayer(Player player);

    HttpStatus deleteAll();
}
