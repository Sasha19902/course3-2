package com.aleksander.wfinal.web.dto.response.meta;


import com.aleksander.wfinal.core.model.team.Team;

public class TeamMetaData {
    private String path;
    private String name;

    public TeamMetaData() { }

    public TeamMetaData(Team team) {
        name = team.getName();
        path = "/team/" + team.getId();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
