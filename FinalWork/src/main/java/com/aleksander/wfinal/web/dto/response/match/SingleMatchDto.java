package com.aleksander.wfinal.web.dto.response.match;

import com.aleksander.wfinal.core.model.match.GameStatistic;
import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.web.dto.response.meta.TeamMetaData;

import java.io.Serializable;

public class SingleMatchDto implements Serializable {
    private long id;
    private TeamMetaData firstTeam;
    private TeamMetaData secondTeam;
    private GameStatistic gameStatistic;

    public SingleMatchDto() { }

    public SingleMatchDto(Match match) {
        this.id = match.getId();
        this.firstTeam = new TeamMetaData(match.getFirstTeam());
        this.secondTeam = new TeamMetaData(match.getSecondTeam());
        this.gameStatistic = match.getGameStatistic();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TeamMetaData getFirstTeam() {
        return firstTeam;
    }

    public void setFirstTeam(TeamMetaData firstTeam) {
        this.firstTeam = firstTeam;
    }

    public TeamMetaData getSecondTeam() {
        return secondTeam;
    }

    public void setSecondTeam(TeamMetaData secondTeam) {
        this.secondTeam = secondTeam;
    }

    public GameStatistic getGameStatistic() {
        return gameStatistic;
    }

    public void setGameStatistic(GameStatistic gameStatistic) {
        this.gameStatistic = gameStatistic;
    }
}
