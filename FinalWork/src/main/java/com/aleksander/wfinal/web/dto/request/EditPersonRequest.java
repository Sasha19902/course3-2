package com.aleksander.wfinal.web.dto.request;

import com.aleksander.wfinal.core.model.person.Person;
import com.aleksander.wfinal.core.services.common.CommonService;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class EditPersonRequest {
    @NotBlank(message = "")
    private String firstName;
    @NotBlank(message = "")
    private String secondName;
    @NotNull
    @Positive(message = "")
    private Long countryId;

    public EditPersonRequest(String firstName,
                             String secondName,
                             Long countryId) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.countryId = countryId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Person editedPerson(Person person, CommonService commonService) {
        person.setFirstName(firstName);
        person.setSecondName(secondName);
        person.setCountry(commonService.getCountryById(countryId));
        return person;
    }
}
