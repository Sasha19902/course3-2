package com.aleksander.wfinal.web.controllers;

import com.aleksander.wfinal.core.model.coach.Coach;
import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.core.services.SpecialDataReturn;
import com.aleksander.wfinal.core.services.coach.CoachService;
import com.aleksander.wfinal.core.services.common.CommonService;
import com.aleksander.wfinal.core.services.team.TeamService;
import com.aleksander.wfinal.core.services.validation.SimpleValidationService;
import com.aleksander.wfinal.web.dto.request.CreateCoachRequest;
import com.aleksander.wfinal.web.dto.request.CreatePlayerRequest;
import com.aleksander.wfinal.web.dto.response.ManyDto;
import com.aleksander.wfinal.web.dto.response.coach.ManyCoachDto;
import com.aleksander.wfinal.web.dto.response.coach.SingleCoachDto;
import com.aleksander.wfinal.web.dto.response.meta.PagingMeta;
import com.aleksander.wfinal.web.dto.response.player.ManyPlayerDto;
import com.aleksander.wfinal.web.dto.response.player.SinglePlayerDto;
import com.aleksander.wfinal.web.dto.response.team.ManyTeamDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collection;
import java.util.stream.Collectors;

@Controller
@RequestMapping("coach/")
public class CoachController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CoachController.class);

    private CoachService coachService;
    private TeamService teamService;
    private CommonService commonService;
    private SimpleValidationService simpleValidationService;

    public CoachController(CoachService coachService, TeamService teamService, CommonService commonService, SimpleValidationService simpleValidationService) {
        this.coachService = coachService;
        this.teamService = teamService;
        this.commonService = commonService;
        this.simpleValidationService = simpleValidationService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getAllCoaches(
            @RequestParam(name = "order", defaultValue = "asc") final String order,
            @RequestParam(name = "page", defaultValue = "1") final int page,
            @RequestParam(name = "size", defaultValue = "25") final int size) {

        if (simpleValidationService.validRequestPage(page) &&
                simpleValidationService.validSizePage(size) &&
                simpleValidationService.validTypeOrder(order)) {
            SpecialDataReturn<Coach> coaches = coachService.getAll(order, page, size);
            PagingMeta pagingMeta = new PagingMeta("coach",
                    coaches.getAllCount(), page, size, order);
            ManyDto<ManyCoachDto> playerManyDto = new ManyDto<>(pagingMeta,
                    coaches.getData().stream().map(ManyCoachDto::new).collect(Collectors.toList()));
            LOGGER.info("CoachController: get all coaches");
            return ResponseEntity.ok(playerManyDto);
        }
        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> addCoach(@Valid @RequestBody CreateCoachRequest createCoachRequest,
                                       BindingResult bindingResult,
                                       Model model) {
        if (bindingResult.hasErrors()) {
            model.addAllAttributes(ControllerUtils.getErrors(bindingResult));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } else {
            Coach coach = createCoachRequest.getCoachFromRequest(commonService);
            coachService.addCoach(coach);
            URI location = UriComponentsBuilder.fromPath("/coach/")
                    .path(Long.toString(coach.getId()))
                    .build()
                    .toUri();
            LOGGER.info(String.format("CoachController: add to database coach with id: %d", coach.getId()));
            return ResponseEntity.created(location).build();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getCoach(@PathVariable("id") final String path) {
        Coach coach = coachService.getById(Long.parseLong(path));
        if (coach != null) {
            return ResponseEntity.ok(new SingleCoachDto(coach));
        }
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> deleteCoach(@PathVariable("id") final String path) {
        Coach coach = coachService.getById(Long.parseLong(path));
        if (coach != null) {
            coachService.deleteCoach(Long.parseLong(path));
            return ResponseEntity.accepted().build();
        }
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/{id}/coach_team", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> coachTeam(@PathVariable("id") final String path) {
        Coach coach = coachService.getById(Long.parseLong(path));
        if (coach != null) {
            Collection<ManyTeamDto> teams = null;
            if(coach.getTeams() != null && !coach.getTeams().isEmpty()) {
                teams = coach.getTeams().stream().map(ManyTeamDto::new).collect(Collectors.toList());
            }
            return ResponseEntity.ok(teams);
        }
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/{id}/coach_team/", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> addCoachTeam(@PathVariable("id") final String path, @RequestBody Long teamId) {
        Coach coach = coachService.getById(Long.parseLong(path));
        Team team = teamService.getById(teamId);
        if (coach != null && team != null) {
            if(team.getCoach() == null) {
                coachService.addTeam(coach.getId(), team);
                return ResponseEntity.ok().build();
            }
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/{id}/coach_team/", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> removeCoachTeam(@PathVariable("id") final String path, @RequestBody Long teamId) {
        Coach coach = coachService.getById(Long.parseLong(path));
        Team team = teamService.getById(teamId);
        if (coach != null && team != null) {
            if(team.getCoach() != null && team.getCoach().getId() == coach.getId()) {
                coachService.removeTeam(team);
                return ResponseEntity.accepted().build();
            }
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> deleteAll() {
        coachService.deleteAll();
        return ResponseEntity.accepted().build();
    }

    @ExceptionHandler(NumberFormatException.class)
    @ResponseBody
    public ResponseEntity<?> casting() {
        return ResponseEntity.badRequest().build();
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<?> handler() {
        return ResponseEntity.badRequest().build();
    }
}
