package com.aleksander.wfinal.web.client.implementation;

import com.aleksander.wfinal.web.client.interfaces.CoachClient;
import com.aleksander.wfinal.core.model.coach.Coach;
import com.aleksander.wfinal.web.dto.request.CreateCoachRequest;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

public class CoachClientImpl implements CoachClient {

    private static final String ROOT_URI = "http://localhost:8080/coach/";
    private RestTemplate restTemplate;

    public CoachClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public URI addCoach(CreateCoachRequest createCoachRequest) {
        URI response = restTemplate.postForLocation(ROOT_URI, createCoachRequest);
        return response;
    }

    @Override
    public Coach getById(long id) {
        return null;
    }
}
