package com.aleksander.wfinal.config;

import com.aleksander.wfinal.core.database.dao.interfaces.TeamDao;
import com.aleksander.wfinal.core.database.impl.TeamDaoImpl;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TeamServiceConfiguration {
    @Bean
    @Qualifier("TeamDao")
    public TeamDao teamDao(@Qualifier("SessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new TeamDaoImpl(sqlSessionFactory);
    }
}
