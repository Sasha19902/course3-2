package com.aleksander.wfinal.config;

import com.aleksander.wfinal.core.database.dao.interfaces.PersonDao;
import com.aleksander.wfinal.core.database.impl.PersonDaoImpl;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersonServiceConfiguration {
    @Bean
    @Qualifier("PersonDao")
    public PersonDao personDao(@Qualifier("SessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new PersonDaoImpl(sqlSessionFactory);
    }
}
