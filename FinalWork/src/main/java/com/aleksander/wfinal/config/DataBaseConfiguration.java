package com.aleksander.wfinal.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;

@Configuration
@ComponentScan("com.aleksander.wfinal")
@MapperScan("com.aleksander.wfinal.core.database.mappers")
public class DataBaseConfiguration {
    @Bean
    @Qualifier("DataSource")
    @ConfigurationProperties(prefix = "spring.datasource.football")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @Qualifier("SessionFactory")
    @ConfigurationProperties(prefix = "connect")
    public SqlSessionFactory sessionFactory(@Qualifier("DataSource") final DataSource dataSource) throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        factoryBean.setMapperLocations(resolver.getResources("classpath:com.aleksander.wfinal.core.database.mappers/*.xml"));
        return factoryBean.getObject();
    }
}
