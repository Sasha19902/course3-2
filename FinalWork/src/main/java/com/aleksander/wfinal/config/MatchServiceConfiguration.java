package com.aleksander.wfinal.config;

import com.aleksander.wfinal.core.database.dao.interfaces.MatchDao;
import com.aleksander.wfinal.core.database.impl.MatchDaoImpl;
import com.aleksander.wfinal.core.model.match.MatchPlayer;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MatchServiceConfiguration {
    @Bean
    @Qualifier("MatchPlayer")
    public MatchPlayer matchPlayer() {
        return new MatchPlayer();
    }

    @Bean
    @Qualifier("MatchDao")
    public MatchDao matchDao(@Qualifier("SessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new MatchDaoImpl(sqlSessionFactory);
    }
}
