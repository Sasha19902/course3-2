package com.aleksander.wfinal.config;

import com.aleksander.wfinal.core.database.dao.interfaces.PlayerDao;
import com.aleksander.wfinal.core.database.impl.PlayerDaoImpl;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PlayerServiceConfiguration {
    @Bean
    @Qualifier("PlayerDao")
    public PlayerDao playerDao(@Qualifier("SessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new PlayerDaoImpl(sqlSessionFactory);
    }
}
