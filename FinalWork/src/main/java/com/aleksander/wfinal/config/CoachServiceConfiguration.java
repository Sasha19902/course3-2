package com.aleksander.wfinal.config;

import com.aleksander.wfinal.core.database.dao.interfaces.CoachDao;
import com.aleksander.wfinal.core.database.impl.CoachDaoImpl;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CoachServiceConfiguration {
    @Bean
    @Qualifier("CoachDao")
    public CoachDao coachDao(@Qualifier("SessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new CoachDaoImpl(sqlSessionFactory);
    }
}
