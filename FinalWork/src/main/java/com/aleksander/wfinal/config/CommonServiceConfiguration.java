package com.aleksander.wfinal.config;

import com.aleksander.wfinal.core.database.dao.interfaces.CommonDao;
import com.aleksander.wfinal.core.database.impl.CommonDaoImpl;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommonServiceConfiguration {
    @Bean
    @Qualifier("CommonDao")
    public CommonDao commonDao(@Qualifier("SessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new CommonDaoImpl(sqlSessionFactory);
    }
}
