package com.aleksander.wfinal;

import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.web.client.implementation.CoachClientImpl;
import com.aleksander.wfinal.web.client.implementation.MatchClientImpl;
import com.aleksander.wfinal.web.client.implementation.PlayerClientImpl;
import com.aleksander.wfinal.web.client.implementation.TeamClientImpl;
import com.aleksander.wfinal.web.client.interfaces.CoachClient;
import com.aleksander.wfinal.web.client.interfaces.MatchClient;
import com.aleksander.wfinal.web.client.interfaces.PlayerClient;
import com.aleksander.wfinal.web.client.interfaces.TeamClient;
import com.aleksander.wfinal.web.dto.request.*;
import com.aleksander.wfinal.web.dto.response.ManyDto;
import com.aleksander.wfinal.web.dto.response.match.SingleMatchDto;
import com.aleksander.wfinal.web.dto.response.player.ManyPlayerDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collection;

@SpringBootApplication
public class Application {

    private static RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(new ObjectMapper());
        restTemplate.getMessageConverters().add(converter);
        return restTemplate;
    }

    private static PlayerClient playerClient(RestTemplate restTemplate) {
        return new PlayerClientImpl(restTemplate);
    }

    private static CoachClient coachClient(RestTemplate restTemplate) {
        return new CoachClientImpl(restTemplate);
    }

    private static TeamClient teamClient(RestTemplate restTemplate) {
        return new TeamClientImpl(restTemplate);
    }

    private static MatchClient matchClient(RestTemplate restTemplate) {
        return new MatchClientImpl(restTemplate);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        RestTemplate restTemplate = restTemplate();
        PlayerClient playerClient = playerClient(restTemplate);
        CoachClient coachClient = coachClient(restTemplate);
        TeamClient  teamClient = teamClient(restTemplate);
        MatchClient matchClient = matchClient(restTemplate);
        /*CREATING PLAYERS*/
        CreatePlayerRequest createPlayerRequest1 = new CreatePlayerRequest("Player:1", "SNP:1", 1L);
        CreatePlayerRequest createPlayerRequest2 = new CreatePlayerRequest("Player:2", "SNP:2", 1L);
        CreatePlayerRequest createPlayerRequest3 = new CreatePlayerRequest("", "SNP:??", 1L);
        CreatePlayerRequest createPlayerRequest4 = new CreatePlayerRequest("Player:3", "SNP:3", 2L);
        CreatePlayerRequest createPlayerRequest5 = new CreatePlayerRequest("Player:4", "SNP:4", 2L);

        URI playerLocation1 = playerClient.addPlayer(createPlayerRequest1);
        URI playerLocation2 = playerClient.addPlayer(createPlayerRequest2);
        URI playerLocation3 = playerClient.addPlayer(createPlayerRequest4);
        URI playerLocation4 = playerClient.addPlayer(createPlayerRequest5);

        /*BAD REQUEST INSERT PLAYER*/
        try {
            URI playerBadRequest = playerClient.addPlayer(createPlayerRequest3);
        } catch (RuntimeException ex) {
            System.out.println("Bad insert player request");
        }
        /*BAD REQUEST INSERT PLAYER\*/
        /*CREATING PLAYERS\*/

        /*CREATING COACHES*/
        CreateCoachRequest createCoachRequest1 = new CreateCoachRequest("Coach:1", "SNC:1", 1L);
        CreateCoachRequest createCoachRequest2 = new CreateCoachRequest("Coach:2", "SNC:2", 2L);
        CreateCoachRequest createCoachRequest3 = new CreateCoachRequest("", "SNC:*??", 2L);

        URI coachLocation1 = coachClient.addCoach(createCoachRequest1);
        URI coachLocation2 = coachClient.addCoach(createCoachRequest2);

        /*BAD REQUEST INSERT COACH*/
        try {
            URI coachLocation3 = coachClient.addCoach(createCoachRequest3);
        } catch (RuntimeException ex) {
            System.out.println("Bad insert coach request");
        }
        /*BAD REQUEST INSERT COACH\*/
        /*CREATING COACHES\*/

        /*CREATING TEAMS*/
        CreateTeamModel createTeamModel1 = new CreateTeamModel("Team:1", 1L);
        CreateTeamModel createTeamModel2 = new CreateTeamModel("Team:2", 2L);
        CreateTeamModel createTeamModel3 = new CreateTeamModel("", 2L);

        URI teamLocation1 = teamClient.addTeam(createTeamModel1);
        URI teamLocation2 = teamClient.addTeam(createTeamModel2);


        /*BAD REQUEST INSERT TEAM*/
        try {
            URI teamLocation3 = teamClient.addTeam(createTeamModel3);
        } catch (RuntimeException ex) {
            System.out.println("Bad insert team request");
        }
        /*BAD REQUEST INSERT COACH\*/


        /*CREATING TEAMS\*/

        /*Entity ID*/
        long playerId1 = Long.parseLong(playerLocation1.toString().replace("/player/", ""));
        long playerId2 = Long.parseLong(playerLocation2.toString().replace("/player/", ""));
        long playerId3 = Long.parseLong(playerLocation3.toString().replace("/player/", ""));
        long playerId4 = Long.parseLong(playerLocation4.toString().replace("/player/", ""));

        long coachId1 = Long.parseLong(coachLocation1.toString().replace("/coach/", ""));
        long coachId2 = Long.parseLong(coachLocation2.toString().replace("/coach/", ""));

        long teamId1 = Long.parseLong(teamLocation1.toString().replace("/team/", ""));
        long teamId2 = Long.parseLong(teamLocation2.toString().replace("/team/", ""));
        /*Entity ID\*/


        /*SET COACHES TO TEAMS*/
        EditTeamRequest editTeamRequest1 = new EditTeamRequest(coachId1, 3L, "Changed_team:1");
        EditTeamRequest editTeamRequest2 = new EditTeamRequest(coachId2, null, null);
        teamClient.updateTeam(teamId1, editTeamRequest1);
        teamClient.updateTeam(teamId2, editTeamRequest2);
        /*SET COACHES TO TEAMS\*/

        /*ADD PLAYERS TO TEAM*/
        teamClient.addPlayerToTeam(teamId1, playerId1);
        teamClient.addPlayerToTeam(teamId1, playerId2);
        teamClient.addPlayerToTeam(teamId2, playerId3);
        teamClient.addPlayerToTeam(teamId2, playerId4);
        /*ADD PLAYERS TO TEAM\*/


        /*CREATING MATCHES*/
        CreateMatchRequest createMatchRequest = new CreateMatchRequest(teamId1, teamId2);
        URI matchLocation1 = matchClient.addMatch(createMatchRequest);
        URI matchLocation2 = matchClient.addMatch(createMatchRequest);

        long matchId1 = Long.parseLong(matchLocation1.toString().replace("/match/", ""));
        long matchId2 = Long.parseLong(matchLocation2.toString().replace("/match/", ""));
        /*CREATING MATCHES\*/

        /*PLAY MATCHES*/
        matchClient.playMatch(matchId1);
        matchClient.playMatch(matchId2);
        /*PLAY MATCHES\*/

        /*CHECK PLAYED MATCHES*/
        SingleMatchDto singleMatchDto1 = matchClient.getById(matchId1);
        SingleMatchDto singleMatchDto2 = matchClient.getById(matchId2);
        System.out.println("MATCH BETWEEN : " + singleMatchDto1.getFirstTeam().getName() + " and " + singleMatchDto1.getSecondTeam().getName() + " WITH SCORE " + singleMatchDto1.getGameStatistic().getLeftTeamScore() + ":" +singleMatchDto1.getGameStatistic().getRightTeamScore());
        System.out.println("MATCH BETWEEN : " + singleMatchDto2.getFirstTeam().getName() + " and " + singleMatchDto2.getSecondTeam().getName() + " WITH SCORE " + singleMatchDto2.getGameStatistic().getLeftTeamScore() + ":" +singleMatchDto2.getGameStatistic().getRightTeamScore());
        /*CHECK PLAYED MATCHES\*/

        /*CHECK PLAYERS*/
        ManyDto<ManyPlayerDto> playerManyDto = playerClient.getAll("asc", 1, 100);

        for (ManyPlayerDto player: playerManyDto.getJsonSerializableData()) {
            System.out.println("PLAYER: " + player.getName());
        }
        /*CHECK PLAYERS\*/

        /*CHECK MATCHES PLAYED BY TEAM*/
        ManyDto<SingleMatchDto> singleMatchDtoManyDto = teamClient.getAll(teamId1);
        for (SingleMatchDto match: singleMatchDtoManyDto.getJsonSerializableData()) {
            System.out.println("MATCH WITH PLAYED BY " + teamId1 + " :"+ match.getId());
        }
        /*CHECK MATCHES PLAYED BY TEAM\*/
    }
}
