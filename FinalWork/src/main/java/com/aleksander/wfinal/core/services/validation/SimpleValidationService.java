package com.aleksander.wfinal.core.services.validation;

import org.springframework.stereotype.Service;

@Service
public class SimpleValidationService {
    private static final String[] TYPE_ORDERS = {"asc", "desc"};

    public boolean validRequestPage(int num) {
        return num > 0;
    }

    public boolean validSizePage(int sz) {
        return sz > 0;
    }

    public boolean validTypeOrder(String ord) {
        return ord.equalsIgnoreCase(TYPE_ORDERS[0]) || ord.equalsIgnoreCase(TYPE_ORDERS[1]);
    }
}
