package com.aleksander.wfinal.core.database.impl;

import com.aleksander.wfinal.core.database.dao.interfaces.PersonDao;
import com.aleksander.wfinal.core.model.person.Person;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.Collection;

public class PersonDaoImpl extends BaseDao implements PersonDao {

    public PersonDaoImpl(SqlSessionFactory sqlSessionFactory) {
        super(sqlSessionFactory);
    }

    @Override
    public void updatePersonData(Person person) {
        personMapper(getSqlSession()).updatePerson(person);
    }

    @Override
    public Person get(Long id) {
        return personMapper(getSqlSession()).getPersonById(id);
    }

    @Override
    public void insert(Person value) {
        personMapper(getSqlSession()).insertPerson(value);
    }

    @Override
    public void delete(Long key) {
        personMapper(getSqlSession()).deletePerson(key);
    }

    @Override
    public void update(Person value) {
        personMapper(getSqlSession()).updatePerson(value);
    }

    @Override
    public void deleteAll() {
        personMapper(getSqlSession()).removeAll();
    }

    @Override
    public Collection<Person> getAll(String order, int offset, int size) {
        return personMapper(getSqlSession()).getAll(order, offset, size);
    }

    @Override
    public long count() {
        return personMapper(getSqlSession()).count();
    }
}
