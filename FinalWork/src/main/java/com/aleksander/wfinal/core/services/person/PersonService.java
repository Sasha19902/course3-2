package com.aleksander.wfinal.core.services.person;

import com.aleksander.wfinal.core.model.person.Person;

public interface PersonService {
    Person  getById(long id);
    void    updatePerson(Person player);
    void    deleteAll();
}
