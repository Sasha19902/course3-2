package com.aleksander.wfinal.core.database.dao.interfaces;

import com.aleksander.wfinal.core.database.dao.comint.CRUDEntity;
import com.aleksander.wfinal.core.model.player.Player;

public interface PlayerDao extends CRUDEntity<Long, Player> {
}

