package com.aleksander.wfinal.core.services.coach;

import com.aleksander.wfinal.core.database.dao.interfaces.CoachDao;
import com.aleksander.wfinal.core.model.coach.Coach;
import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.core.services.SpecialDataReturn;
import org.springframework.stereotype.Service;

@Service
public class CoachServiceImpl implements CoachService {

    private final CoachDao coachDao;

    public CoachServiceImpl(CoachDao coachDao) {
        this.coachDao = coachDao;
    }

    @Override
    public SpecialDataReturn<Coach> getAll(String order, int page, int size) {
        return new SpecialDataReturn<>(coachDao.getAll(order, page, size), coachDao.count());
    }

    @Override
    public void addCoach(Coach coach) {
        coachDao.insert(coach);
    }

    @Override
    public Coach getById(long id) {
        return coachDao.get(id);
    }

    @Override
    public void deleteCoach(long id) {
        coachDao.delete(id);
    }

    @Override
    public void updateCoach(Coach coach) {
        coachDao.update(coach);
    }

    @Override
    public void deleteAll() {
        coachDao.deleteAll();
    }

    @Override
    public void addTeam(long id, Team team) {
        coachDao.addTeam(id, team);
    }

    @Override
    public void removeTeam(Team team) {
        coachDao.removeTeam(team);
    }
}
