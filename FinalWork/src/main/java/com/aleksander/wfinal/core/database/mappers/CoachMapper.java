package com.aleksander.wfinal.core.database.mappers;

import com.aleksander.wfinal.core.model.coach.Coach;
import com.aleksander.wfinal.core.model.team.Team;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;

public interface CoachMapper {
    void insertCoach(@Param("coach") Coach coach);

    Coach getCoachById(@Param("id") long id);

    void removeAll();

    Collection<Coach> getAll(@Param("order") String order, @Param("offset") int offset, @Param("size") int size);

    long count();

    void addTeam(@Param("coachId") long coachId,@Param("team") Team team);

    void removeTeam(@Param("team") Team team);
}
