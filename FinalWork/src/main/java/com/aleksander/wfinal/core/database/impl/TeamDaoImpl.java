package com.aleksander.wfinal.core.database.impl;

import com.aleksander.wfinal.core.database.dao.interfaces.TeamDao;
import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.core.model.team.Team;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

public class TeamDaoImpl extends BaseDao implements TeamDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeamDaoImpl.class);


    public TeamDaoImpl(SqlSessionFactory sqlSessionFactory) {
        super(sqlSessionFactory);
    }

    @Override
    public void addPlayerToTeam(Long teamId, Long playerId) {
        teamMapper(getSqlSession()).addPlayerToTeam(teamId, playerId);
    }

    @Override
    public void removePlayerFromTeam(Long teamId, Long playerId) {
        teamMapper(getSqlSession()).removePlayerFromTeam(teamId, playerId);
    }

    @Override
    public Collection<Match> matchesPlayedByTeam(Team team, String order, int offset, int size) {
        return teamMapper(getSqlSession()).matchesPlayedByTeam(team, order, offset, size);
    }

    @Override
    public long countMatchesPlayedByTeam(Team team) {
        return teamMapper(getSqlSession()).countMatchesPlayedByTeam(team);
    }

    @Override
    public Team get(Long id) {
        return teamMapper(getSqlSession()).getTeamById(id);
    }

    @Override
    public void insert(Team value) {
        teamMapper(getSqlSession()).insertTeam(value);
    }

    @Override
    public void delete(Long key) {
        teamMapper(getSqlSession()).removeTeam(key);
    }

    @Override
    public void update(Team value) {
        teamMapper(getSqlSession()).updateTeam(value);
    }

    @Override
    public void deleteAll() {
        teamMapper(getSqlSession()).removeAll();
    }

    @Override
    public Collection<Team> getAll(String order, int offset, int size) {
        return teamMapper(getSqlSession()).getAll(order, offset, size);
    }

    @Override
    public long count() {
        return teamMapper(getSqlSession()).count();
    }
}
