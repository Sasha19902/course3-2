package com.aleksander.wfinal.core.model.match;


import com.aleksander.wfinal.core.model.team.Team;

import java.io.Serializable;
import java.util.Objects;

public class Match implements Serializable {
    private long id;
    private Team firstTeam;
    private Team secondTeam;
    private GameStatistic gameStatistic;

    public Match() { }

    public Match(long id, Team firstTeam, Team secondTeam) {
        this(id, firstTeam, secondTeam, null);
    }

    public Match(long id, Team firstTeam, Team secondTeam, GameStatistic gameStatistic) {
        this.id = id;
        this.firstTeam = firstTeam;
        this.secondTeam = secondTeam;
        this.gameStatistic = gameStatistic;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Team getFirstTeam() {
        return firstTeam;
    }

    public void setFirstTeam(Team firstTeam) {
        this.firstTeam = firstTeam;
    }

    public Team getSecondTeam() {
        return secondTeam;
    }

    public void setSecondTeam(Team secondTeam) {
        this.secondTeam = secondTeam;
    }

    public GameStatistic getGameStatistic() {
        return gameStatistic;
    }

    public void setGameStatistic(GameStatistic gameStatistic) {
        this.gameStatistic = gameStatistic;
    }
}
