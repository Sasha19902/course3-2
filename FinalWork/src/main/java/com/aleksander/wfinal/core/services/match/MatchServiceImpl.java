package com.aleksander.wfinal.core.services.match;

import com.aleksander.wfinal.core.database.dao.interfaces.MatchDao;
import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.core.model.match.MatchPlayer;
import com.aleksander.wfinal.core.services.SpecialDataReturn;
import org.springframework.stereotype.Service;

@Service
public class MatchServiceImpl implements MatchService {

    private final MatchDao matchDao;
    private final MatchPlayer matchPlayer;

    public MatchServiceImpl(MatchDao matchDao, MatchPlayer matchPlayer) {
        this.matchDao = matchDao;
        this.matchPlayer = matchPlayer;
    }

    @Override
    public SpecialDataReturn<Match> getAll(String order, int page, int size) {
        return new SpecialDataReturn<>(matchDao.getAll(order, (page - 1) * size, size), matchDao.count());
    }

    @Override
    public void addMatch(Match match) {
        matchDao.insert(match);
    }

    @Override
    public Match getById(long id) {
        return matchDao.get(id);
    }

    @Override
    public void deleteMatch(long id) {
        matchDao.delete(id);
    }

    @Override
    public void updateMatch(Match match) {
        matchDao.update(match);
    }

    @Override
    public void deleteAll() {
        matchDao.deleteAll();
    }

    @Override
    public void playMatch(Match match) {
        matchPlayer.playMatch(match);
        matchDao.playMatch(match);
    }
}
