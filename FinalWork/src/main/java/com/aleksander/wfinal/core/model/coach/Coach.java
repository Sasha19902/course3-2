package com.aleksander.wfinal.core.model.coach;

import com.aleksander.wfinal.core.model.country.Country;
import com.aleksander.wfinal.core.model.person.Person;
import com.aleksander.wfinal.core.model.team.Team;

import java.util.List;

public class Coach extends Person {
    private List<Team> teams;

    public Coach() { }

    public Coach(long id, String firstName, String secondName, Country country) {
        this(id, firstName, secondName, country, null);
    }

    public Coach(long id, String firstName, String secondName, Country country, List<Team> teams) {
        super(id, firstName, secondName, country);
        this.teams = teams;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }
}
