package com.aleksander.wfinal.core.database.impl;

import com.aleksander.wfinal.core.database.dao.interfaces.CoachDao;
import com.aleksander.wfinal.core.model.coach.Coach;
import com.aleksander.wfinal.core.model.team.Team;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.Collection;

public class CoachDaoImpl extends BaseDao implements CoachDao {
    public CoachDaoImpl(SqlSessionFactory sqlSessionFactory) {
        super(sqlSessionFactory);
    }

    @Override
    public Coach get(Long id) {
        return coachMapper(getSqlSession()).getCoachById(id);
    }

    @Override
    public void insert(Coach value) {
        personMapper(getSqlSession()).insertPerson(value);
        coachMapper(getSqlSession()).insertCoach(value);
    }

    @Override
    public void delete(Long key) {
        personMapper(getSqlSession()).deletePerson(key);
    }

    @Override
    public void update(Coach value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteAll() {
        coachMapper(getSqlSession()).removeAll();
    }

    @Override
    public Collection<Coach> getAll(String order, int offset, int size) {
        return coachMapper(getSqlSession()).getAll(order, offset, size);
    }

    @Override
    public long count() {
        return coachMapper(getSqlSession()).count();
    }

    @Override
    public void addTeam(long coachId, Team team) {
        coachMapper(getSqlSession()).addTeam(coachId, team);
    }

    @Override
    public void removeTeam(Team team) {
        coachMapper(getSqlSession()).removeTeam(team);
    }
}
