package com.aleksander.wfinal.core.database.dao.interfaces;


import com.aleksander.wfinal.core.database.dao.comint.CRUDEntity;
import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.core.model.team.Team;

import java.util.Collection;

public interface TeamDao extends CRUDEntity<Long, Team> {
    void addPlayerToTeam(Long teamId, Long playerId);
    void removePlayerFromTeam(Long teamId, Long playerId);
    Collection<Match> matchesPlayedByTeam(Team team, String order, int offset, int size);
    long countMatchesPlayedByTeam(Team team);
}
