package com.aleksander.wfinal.core.database.dao.interfaces;

import com.aleksander.wfinal.core.database.dao.comint.CRUDEntity;
import com.aleksander.wfinal.core.model.coach.Coach;
import com.aleksander.wfinal.core.model.team.Team;

import java.util.Collection;

public interface CoachDao extends CRUDEntity<Long, Coach> {
    void addTeam(long coachId, Team team);
    void removeTeam(Team team);

}
