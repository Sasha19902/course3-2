package com.aleksander.wfinal.core.model.team;

import com.aleksander.wfinal.core.model.coach.Coach;
import com.aleksander.wfinal.core.model.country.Country;
import com.aleksander.wfinal.core.model.player.Player;

import java.util.List;
import java.util.Objects;

public class Team {
    private long id;
    private String name;
    private Country country;
    private List<Player> playerList;
    private Coach coach;

    public Team() { }

    public Team(long id, String name, Country country) {
        this(id, name, null, null, country);
    }

    public Team(long id, String name, List<Player> playerList, Coach coach, Country country) {
        this.id = id;
        this.name = name;
        this.playerList = playerList;
        this.coach = coach;
        this.country = country;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Team)) return false;
        Team team = (Team) o;
        return getId() == team.getId() &&
                Objects.equals(getName(), team.getName()) &&
                Objects.equals(getPlayerList(), team.getPlayerList()) &&
                Objects.equals(getCoach(), team.getCoach()) &&
                Objects.equals(getCountry(), team.getCountry());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getPlayerList(), getCoach(), getCountry());
    }
}
