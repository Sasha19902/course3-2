package com.aleksander.wfinal.core.database.dao.comint;

import java.util.Collection;

public interface CRUDEntity<K, V> {
    V    get(K id);
    void insert(V value);
    void delete(K key);
    void update(V value);
    void deleteAll();
    Collection<V> getAll(String order, int offset, int size);
    long count();
}
