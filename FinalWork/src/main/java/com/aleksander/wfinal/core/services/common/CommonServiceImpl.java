package com.aleksander.wfinal.core.services.common;

import com.aleksander.wfinal.core.database.dao.interfaces.CommonDao;
import com.aleksander.wfinal.core.model.country.Country;
import com.aleksander.wfinal.core.model.player.Position;
import com.aleksander.wfinal.core.services.SpecialDataReturn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service
public class CommonServiceImpl implements CommonService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonServiceImpl.class);
    public final CommonDao commonDao;

    public CommonServiceImpl(CommonDao commonDao) {
        this.commonDao = commonDao;
    }

    @Override
    public Position getPositionById(long id) {
        LOGGER.info(String.format("CommonServiceImpl: position getPositionById with params: %d", id));
        return commonDao.getPositionById(id);
    }

    @Override
    public Collection<Position> positions() {
        LOGGER.info("CommonServiceImpl: get positions with params: %d");
        return commonDao.positions();
    }

    @Override
    public Country getCountryById(long id) {
        LOGGER.info(String.format("CommonServiceImpl: get country with id = %d", id));
        return commonDao.getCountryById(id);
    }

    @Override
    public SpecialDataReturn<Country> getAllCountries(String order, long page, long size) {
        LOGGER.info(String.format("CommonServiceImpl: get countries with params: {\n order = %s, page = %d, size = %d", order, page, size));
        return new SpecialDataReturn<>(commonDao.getAllCountries(order, page, size), commonDao.countCountries());
    }

}
