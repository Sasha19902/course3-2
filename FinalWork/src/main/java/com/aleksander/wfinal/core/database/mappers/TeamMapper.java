package com.aleksander.wfinal.core.database.mappers;

import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.core.model.team.Team;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;

public interface TeamMapper {
    void insertTeam(@Param("team") Team team);

    Team getTeamById(@Param("id") long id);

    void updateTeam(@Param("team") Team team);

    void removeTeam(@Param("id") long id);

    void removeAll();

    void addPlayerToTeam(@Param("teamId") long teamId, @Param("playerId") long playerId);

    void removePlayerFromTeam(@Param("teamId") long teamId, @Param("playerId") long playerId);

    long count();

    Collection<Team> getAll(@Param("order") String order, @Param("offset") int page, @Param("size") int size);

    Collection<Match> matchesPlayedByTeam(@Param("team") Team team, @Param("order") String order, @Param("offset") int page, @Param("size") int size);

    long countMatchesPlayedByTeam(@Param("team")Team team);
}
