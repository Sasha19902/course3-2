package com.aleksander.wfinal.core.model.player;

import com.aleksander.wfinal.core.model.country.Country;
import com.aleksander.wfinal.core.model.person.Person;
import com.aleksander.wfinal.core.model.team.Team;

import java.util.Objects;

public class Player extends Person {

    private int playerNumber;
    private Position position;
    private Team team;

    public Player() { }

    public Player(long id, String firstName, String secondName, Country country) {
        this(id, firstName, secondName, country, 0, null, null);
    }

    public Player(long id, String firstName, String secondName, Country country, int playerNumber, Position position, Team team) {
        super(id, firstName, secondName, country);
        this.playerNumber = playerNumber;
        this.position = position;
        this.team = team;
    }

    public int getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Player)) return false;
        Player player = (Player) o;
        return getPlayerNumber() == player.getPlayerNumber() &&
                Objects.equals(getPosition(), player.getPosition()) &&
                Objects.equals(getTeam(), player.getTeam());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPlayerNumber(), getPosition(), getTeam());
    }

    @Override
    public String toString() {
        return "Player{" +
                "playerNumber=" + playerNumber +
                ", position=" + position +
                ", team=" + team +
                '}';
    }
}
