package com.aleksander.wfinal.core.services;

import java.util.Collection;

public class SpecialDataReturn<T> {
    private Collection<T> data;
    private long allCount;

    public SpecialDataReturn(Collection<T> data, long allCount) {
        this.data = data;
        this.allCount = allCount;
    }

    public Collection<T> getData() {
        return data;
    }

    public void setData(Collection<T> data) {
        this.data = data;
    }

    public long getAllCount() {
        return allCount;
    }

    public void setAllCount(long allCount) {
        this.allCount = allCount;
    }
}
