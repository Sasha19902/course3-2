package com.aleksander.wfinal.core.services.player;

import com.aleksander.wfinal.core.database.dao.interfaces.PlayerDao;
import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.core.services.SpecialDataReturn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class PlayerServiceImpl implements PlayerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerServiceImpl.class);
    private final PlayerDao playerDao;

    public PlayerServiceImpl(PlayerDao playerDao) {
        this.playerDao = playerDao;
    }

    @Override
    public SpecialDataReturn<Player> getAll(String order, int page, int size) {
        LOGGER.info(String.format("PlayerServiceImpl: getAll with params: { order = %s, page = %d, size = %d }", order, page, size));
        return new SpecialDataReturn<>(playerDao.getAll(order, (page - 1) * size , size), playerDao.count());
    }

    @Override
    public void addPlayer(Player player) {
        LOGGER.info(String.format("PlayerServiceImpl: add player with params : %s", player));
        playerDao.insert(player);
    }

    @Override
    public Player getById(long id) {
        LOGGER.info(String.format("PlayerServiceImpl: get player with id = %d", id));
        return playerDao.get(id);
    }

    @Override
    public void deletePlayer(long id) {
        LOGGER.info(String.format("PlayerServiceImpl: delete player with id = %d", id));
        playerDao.delete(id);
    }

    @Override
    public void updatePlayer(Player player) {
        LOGGER.info(String.format("PlayerServiceImpl: update player : %s", player));
        playerDao.update(player);
    }

    @Override
    public void deleteAll() {
        LOGGER.info("PlayerServiceImpl: delete all");
        playerDao.deleteAll();
    }
}
