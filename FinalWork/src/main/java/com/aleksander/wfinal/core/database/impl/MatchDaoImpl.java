package com.aleksander.wfinal.core.database.impl;

import com.aleksander.wfinal.core.database.dao.interfaces.MatchDao;
import com.aleksander.wfinal.core.model.match.Match;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

public class MatchDaoImpl extends BaseDao implements MatchDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerDaoImpl.class);


    public MatchDaoImpl(SqlSessionFactory sqlSessionFactory) {
        super(sqlSessionFactory);
    }

    @Override
    public void playMatch(Match match) {
        matchMapper(getSqlSession()).playMatch(match);
    }

    @Override
    public Match get(Long id) {
        return matchMapper(getSqlSession()).getMatchById(id);
    }

    @Override
    public void insert(Match value) {
        matchMapper(getSqlSession()).insertMatch(value);
    }

    @Override
    public void delete(Long key) {
        matchMapper(getSqlSession()).removeMatch(key);
    }

    @Override
    public void update(Match value) {
        matchMapper(getSqlSession()).updateMatch(value);
    }

    @Override
    public void deleteAll() {
        matchMapper(getSqlSession()).removeAll();
    }

    @Override
    public Collection<Match> getAll(String order, int offset, int size) {
        return matchMapper(getSqlSession()).getAll(order, offset, size);
    }

    @Override
    public long count() {
        return matchMapper(getSqlSession()).count();
    }
}
