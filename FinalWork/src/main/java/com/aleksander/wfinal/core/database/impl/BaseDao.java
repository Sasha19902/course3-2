package com.aleksander.wfinal.core.database.impl;

import com.aleksander.wfinal.core.database.mappers.*;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;

public abstract class BaseDao extends SqlSessionDaoSupport {

    public BaseDao(SqlSessionFactory sqlSessionFactory) {
        setSqlSessionFactory(sqlSessionFactory);
    }

    protected PlayerMapper playerMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(PlayerMapper.class);
    }

    protected TeamMapper teamMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(TeamMapper.class);
    }

    protected MatchMapper matchMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(MatchMapper.class);
    }

    protected CommonMapper commonMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(CommonMapper.class);
    }

    protected PersonMapper personMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(PersonMapper.class);
    }

    protected CoachMapper coachMapper(SqlSession sqlSession) { return sqlSession.getMapper(CoachMapper.class); }
}
