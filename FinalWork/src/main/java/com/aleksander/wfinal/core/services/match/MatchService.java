package com.aleksander.wfinal.core.services.match;

import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.core.services.SpecialDataReturn;

public interface MatchService {
    SpecialDataReturn<Match> getAll(String order, int page, int size);
    void    addMatch(Match match);
    Match   getById(long id);
    void    deleteMatch(long id);
    void    updateMatch(Match match);
    void    deleteAll();
    void    playMatch(Match match);
}
