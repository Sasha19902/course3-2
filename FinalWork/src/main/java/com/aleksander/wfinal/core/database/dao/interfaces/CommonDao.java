package com.aleksander.wfinal.core.database.dao.interfaces;

import com.aleksander.wfinal.core.model.country.Country;
import com.aleksander.wfinal.core.model.player.Position;

import java.util.Collection;

public interface CommonDao {
    Position getPositionById(long id);
    Collection<Position> positions();
    Country getCountryById(long id);
    Collection<Country> getAllCountries(String order, long offset, long size);
    long countCountries();
}
