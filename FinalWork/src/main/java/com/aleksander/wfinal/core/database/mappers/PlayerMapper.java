package com.aleksander.wfinal.core.database.mappers;

import com.aleksander.wfinal.core.model.player.Player;
import org.apache.ibatis.annotations.*;

import java.util.Collection;


public interface PlayerMapper {
    void insertPlayer(@Param("player") Player player);

    Player getPlayerById(@Param("id") long id);

    void updatePlayer(@Param("player") Player player);

    void removePlayer(@Param("id") long id);

    void removeAll();

    Collection<Player> getAll(@Param("order") String order, @Param("offset") int offset, @Param("size") int size);

    long count();
}
