package com.aleksander.wfinal.core.services.common;

import com.aleksander.wfinal.core.model.country.Country;
import com.aleksander.wfinal.core.model.player.Position;
import com.aleksander.wfinal.core.services.SpecialDataReturn;

import java.util.Collection;
import java.util.List;

public interface CommonService {
    Position getPositionById(long id);
    Collection<Position> positions();
    Country getCountryById(long id);
    SpecialDataReturn<Country> getAllCountries(String order, long page, long size);
}
