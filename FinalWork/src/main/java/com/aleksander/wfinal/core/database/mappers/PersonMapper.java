package com.aleksander.wfinal.core.database.mappers;

import com.aleksander.wfinal.core.model.person.Person;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;

public interface PersonMapper {
    void insertPerson(@Param("person") Person person);

    Person getPersonById(@Param("id") long id);

    void deletePerson(@Param("id") long id);

    void updatePerson(@Param("person") Person person);

    long count();

    Collection<Person> getAll(@Param("order") String order, @Param("offset") int offset, @Param("size") int size);

    void removeAll();
}
