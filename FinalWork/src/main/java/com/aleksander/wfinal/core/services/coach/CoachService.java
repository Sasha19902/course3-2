package com.aleksander.wfinal.core.services.coach;

import com.aleksander.wfinal.core.model.coach.Coach;
import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.core.services.SpecialDataReturn;

public interface CoachService {
    SpecialDataReturn<Coach> getAll(String order, int page, int size);
    void    addCoach(Coach coach);
    Coach   getById(long id);
    void    deleteCoach(long id);
    void    updateCoach(Coach coach);
    void    deleteAll();
    void    addTeam(long id, Team team);
    void    removeTeam(Team team);
}
