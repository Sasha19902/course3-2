package com.aleksander.wfinal.core.services.person;

import com.aleksander.wfinal.core.database.dao.interfaces.PersonDao;
import com.aleksander.wfinal.core.model.person.Person;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl implements PersonService {

    private final PersonDao personDao;

    public PersonServiceImpl(PersonDao personDao) {
        this.personDao = personDao;
    }

    @Override
    public Person getById(long id) {
        return personDao.get(id);
    }

    @Override
    public void updatePerson(Person player) {
        personDao.update(player);
    }

    @Override
    public void deleteAll() {
        personDao.deleteAll();
    }
}
