package com.aleksander.wfinal.core.model.person;

import com.aleksander.wfinal.core.model.country.Country;

public class Person {
    private long id;
    private String firstName;
    private String secondName;
    private Country country;

    public Person() { }

    public Person(long id, String firstName, String secondName, Country country) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.country = country;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
