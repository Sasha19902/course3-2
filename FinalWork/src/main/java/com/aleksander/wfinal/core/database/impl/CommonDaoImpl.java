package com.aleksander.wfinal.core.database.impl;

import com.aleksander.wfinal.core.database.dao.interfaces.CommonDao;
import com.aleksander.wfinal.core.model.country.Country;
import com.aleksander.wfinal.core.model.player.Position;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.Collection;
import java.util.List;

public class CommonDaoImpl extends BaseDao implements CommonDao {

    public CommonDaoImpl(SqlSessionFactory sqlSessionFactory) {
        super(sqlSessionFactory);
    }

    @Override
    public Position getPositionById(long id) {
        return commonMapper(getSqlSession()).getPositionById(id);
    }

    @Override
    public Collection<Position> positions() {
        return commonMapper(getSqlSession()).positions();
    }

    @Override
    public Country getCountryById(long id) {
        return commonMapper(getSqlSession()).getCountryById(id);
    }

    @Override
    public Collection<Country> getAllCountries(String order, long offset, long size) {
        return commonMapper(getSqlSession()).getAllCountries(order, offset, size);
    }

    @Override
    public long countCountries() {
        return commonMapper(getSqlSession()).countCountries();
    }
}
