package com.aleksander.wfinal.core.database.dao.interfaces;

import com.aleksander.wfinal.core.database.dao.comint.CRUDEntity;
import com.aleksander.wfinal.core.model.match.Match;

public interface MatchDao extends CRUDEntity<Long, Match> {
    void  playMatch(Match match);
}
