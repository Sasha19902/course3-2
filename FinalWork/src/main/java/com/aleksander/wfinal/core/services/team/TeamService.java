package com.aleksander.wfinal.core.services.team;

import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.core.services.SpecialDataReturn;
import org.mockito.internal.matchers.Matches;

import java.util.Collection;

public interface TeamService {
    SpecialDataReturn<Team> getAll(String order, int page, int size);
    void addTeam(Team team);
    Team getById(Long key);
    void updateTeam(Team team);
    void deleteTeam(Long key);
    void deleteAll();
    void addPlayerToTeam(Long teamId, Long playerId);
    void removePlayerFromTeam(Long teamId, Long playerId);
    SpecialDataReturn<Match> matchesPlayedByTeam(Team team, String order, int page, int size);
}
