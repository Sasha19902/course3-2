package com.aleksander.wfinal.core.database.mappers;

import com.aleksander.wfinal.core.model.coach.Coach;
import com.aleksander.wfinal.core.model.match.Match;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;

public interface MatchMapper {
    void insertMatch(@Param("match") Match match);

    Match getMatchById(@Param("id") long id);

    void updateMatch(@Param("match") Match match);

    void removeMatch(@Param("id") long id);

    void removeAll();

    Collection<Match> getAll(@Param("order") String order, @Param("offset") int offset, @Param("size") int size);

    long count();

    void playMatch(@Param("match") Match match);
}
