package com.aleksander.wfinal.core.model.match;

import java.util.Random;

public class MatchPlayer {

    public void playMatch(Match match) {
        Random random = new Random();
        GameStatistic gameStatistic = new GameStatistic(random.nextInt(15), random.nextInt(15));
        match.setGameStatistic(gameStatistic);
    }
}
