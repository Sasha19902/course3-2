package com.aleksander.wfinal.core.model.match;

import java.util.Objects;

public class GameStatistic {
    private int leftTeamScore;
    private int rightTeamScore;

    public GameStatistic() {}

    public GameStatistic(int leftTeamScore, int rightTeamScore) {
        this.leftTeamScore = leftTeamScore;
        this.rightTeamScore = rightTeamScore;
    }

    public int getLeftTeamScore() {
        return leftTeamScore;
    }

    public void setLeftTeamScore(int leftTeamScore) {
        this.leftTeamScore = leftTeamScore;
    }

    public int getRightTeamScore() {
        return rightTeamScore;
    }

    public void setRightTeamScore(int rightTeamScore) {
        this.rightTeamScore = rightTeamScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GameStatistic)) return false;
        GameStatistic that = (GameStatistic) o;
        return getLeftTeamScore() == that.getLeftTeamScore() &&
                getRightTeamScore() == that.getRightTeamScore();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLeftTeamScore(), getRightTeamScore());
    }
}
