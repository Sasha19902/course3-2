package com.aleksander.wfinal.core.database.dao.interfaces;

import com.aleksander.wfinal.core.database.dao.comint.CRUDEntity;
import com.aleksander.wfinal.core.model.person.Person;

public interface PersonDao extends CRUDEntity<Long, Person>{
    void updatePersonData(Person person);
}
