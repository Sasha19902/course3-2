package com.aleksander.wfinal.core.services.team;

import com.aleksander.wfinal.core.database.dao.interfaces.TeamDao;
import com.aleksander.wfinal.core.model.match.Match;
import com.aleksander.wfinal.core.model.team.Team;
import com.aleksander.wfinal.core.services.SpecialDataReturn;
import org.springframework.stereotype.Service;

@Service
public class TeamServiceImpl implements TeamService {

    private final TeamDao teamDao;

    public TeamServiceImpl(TeamDao teamDao) {
        this.teamDao = teamDao;
    }

    @Override
    public SpecialDataReturn<Team> getAll(String order, int page, int size) {
        return new SpecialDataReturn<>(teamDao.getAll(order, (page - 1) * size, size), teamDao.count());
    }

    @Override
    public void addTeam(Team team) {
        teamDao.insert(team);
    }

    @Override
    public Team getById(Long key) {
        return teamDao.get(key);
    }

    @Override
    public void updateTeam(Team team) {
        teamDao.update(team);
    }

    @Override
    public void deleteTeam(Long key) {
        teamDao.delete(key);
    }

    @Override
    public void deleteAll() {
        teamDao.deleteAll();
    }

    @Override
    public void addPlayerToTeam(Long teamId, Long playerId) {
        teamDao.addPlayerToTeam(teamId, playerId);
    }

    @Override
    public void removePlayerFromTeam(Long teamId, Long playerId) {
        teamDao.removePlayerFromTeam(teamId, playerId);
    }

    @Override
    public SpecialDataReturn<Match> matchesPlayedByTeam(Team team, String order, int page, int size) {
        return new SpecialDataReturn<>(teamDao.matchesPlayedByTeam(team, order, (page - 1) * size, size), teamDao.countMatchesPlayedByTeam(team));
    }
}
