package com.aleksander.wfinal.core.services.player;

import com.aleksander.wfinal.core.model.player.Player;
import com.aleksander.wfinal.core.services.SpecialDataReturn;

public interface PlayerService {
    SpecialDataReturn<Player> getAll(String order, int page, int size);
    void    addPlayer(Player player);
    Player  getById(long id);
    void    deletePlayer(long id);
    void    updatePlayer(Player player);
    void    deleteAll();
}
