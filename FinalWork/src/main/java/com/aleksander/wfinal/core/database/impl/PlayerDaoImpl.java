package com.aleksander.wfinal.core.database.impl;

import com.aleksander.wfinal.core.database.dao.interfaces.PlayerDao;
import com.aleksander.wfinal.core.model.player.Player;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

public class PlayerDaoImpl extends BaseDao implements PlayerDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerDaoImpl.class);

    public PlayerDaoImpl(SqlSessionFactory sqlSessionFactory) {
        super(sqlSessionFactory);
    }

    @Override
    public Player get(Long id) {
        LOGGER.info(String.format("PlayerDaoImpl: get player with id = %d", id));
        return playerMapper(getSqlSession()).getPlayerById(id);
    }

    @Override
    public void insert(Player player) {
        LOGGER.info("PlayerDaoImpl: insert player");
        personMapper(getSqlSession()).insertPerson(player);
        playerMapper(getSqlSession()).insertPlayer(player);
    }

    @Override
    public void delete(Long key) {
        LOGGER.info(String.format("PlayerDaoImpl: delete player with id = %d",key));
        personMapper(getSqlSession()).deletePerson(key);
    }

    @Override
    public void update(Player player) {
        LOGGER.info(String.format("PlayerDaoImpl: update player with id = %d", player.getId()));
        playerMapper(getSqlSession()).updatePlayer(player);
    }

    @Override
    public void deleteAll() {
        LOGGER.info("PlayerDaoImpl: remove all players");
        playerMapper(getSqlSession()).removeAll();
    }

    @Override
    public Collection<Player> getAll(String order, int offset, int size) {
        LOGGER.info("PlayerDaoImpl: get all player");
        return playerMapper(getSqlSession()).getAll(order, offset, size);
    }

    @Override
    public long count() {
        LOGGER.info("PlayerDaoImpl: get count of players");
        return playerMapper(getSqlSession()).count();
    }
}
