DROP SCHEMA IF EXISTS football;
CREATE SCHEMA football;
USE football;

CREATE TABLE IF NOT EXISTS position
(
	id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(16) NOT NULL,
    PRIMARY KEY(id)
) ENGINE = INNODB;

INSERT INTO position VALUES(null, "Forward"),
			 (null, "Goalkepper"),
			 (null, "Defender"),
			 (null, "Midfielder");

CREATE TABLE IF NOT EXISTS country
(
	id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(16) NOT NULL,
    PRIMARY KEY(id)
) ENGINE = INNODB DEFAULT CHARSET = utf8mb4;

INSERT INTO country VALUES(null, "Russia"),
			 (null, "USA"),
			 (null, "Germany"),
			 (null, "France");


CREATE TABLE IF NOT EXISTS person
(
	id INT AUTO_INCREMENT NOT NULL,
	first_name VARCHAR(32) NOT NULL,
    second_name VARCHAR(32) NOT NULL,
    country_id INT,
    FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE SET NULL,
	PRIMARY KEY(id)
) ENGINE = INNODB DEFAULT CHARSET = utf8mb4;

CREATE TABLE IF NOT EXISTS coach
(
	person_id INT NOT NULL,
	FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE,
	PRIMARY KEY (person_id)
) ENGINE = INNODB DEFAULT CHARSET = utf8mb4;

CREATE TABLE IF NOT EXISTS team
(
	id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(128) NOT NULL,
    coach_id INT,
    country_id INT,
	FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE SET NULL,
	FOREIGN KEY (coach_id) REFERENCES coach (person_id) ON DELETE SET NULL,
    PRIMARY KEY(id)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS player
(
	person_id INT NOT NULL,
	player_number INT,
    position_id INT,
    team_id INT,
    FOREIGN KEY (position_id) REFERENCES position (id) ON DELETE SET NULL,
    FOREIGN KEY (team_id) REFERENCES team (id) ON DELETE SET NULL,
	FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE,
    PRIMARY KEY(person_id)
) ENGINE = INNODB DEFAULT CHARSET = utf8mb4;

CREATE TABLE IF NOT EXISTS football_match
(
	id INT AUTO_INCREMENT NOT NULL,
    first_team_id INT NOT NULL,
    second_team_id INT NOT NULL,
	first_score INT,
    second_score INT,
	FOREIGN KEY (first_team_id) REFERENCES team (id) ON DELETE CASCADE,
	FOREIGN KEY (second_team_id) REFERENCES team (id) ON DELETE CASCADE,
    PRIMARY KEY(id)
) ENGINE = INNODB;