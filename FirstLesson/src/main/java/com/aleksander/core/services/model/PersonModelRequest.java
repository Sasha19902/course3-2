package com.aleksander.core.services.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PersonModelRequest {

    private String firstName;
    private String lastName;
    private int age;

    @JsonCreator
    public PersonModelRequest(@JsonProperty("firstName") String firstName,
                              @JsonProperty("lastName") String lastName,
                              @JsonProperty("age") int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean firstNamePresent() {
        return firstName != null;
    }

    public boolean lastNamePresent() {
        return lastName != null;
    }

    public boolean agePresent() {
        return age != 0;
    }
}
