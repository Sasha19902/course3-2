package com.aleksander.core.services;

import com.aleksander.core.model.Person;
import com.aleksander.core.services.model.PersonModelRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Server {

    private List<Person> persons;
    private Gson gson;

    public Server() {
        persons = new ArrayList<>();
        gson = new Gson();

        persons.add(new Person("ROB_1", "VER_1", 1));
        persons.add(new Person("ROB_2", "VER_2", 2));
        persons.add(new Person("ROB_3", "VER_3", 3));
        persons.add(new Person("ROB_4", "VER_4", 4));
        persons.add(new Person("ROB_5", "VER_5", 5));
    }

    public String get(String request) {
        ObjectMapper objectMapper = new ObjectMapper();
        PersonModelRequest personModelRequest = null;
        String result = null;
        try {
            personModelRequest =  objectMapper.readValue(request, PersonModelRequest.class);

            if(!personModelRequest.firstNamePresent() &&
                    !personModelRequest.lastNamePresent() &&
                    !personModelRequest.agePresent()) {
                return null;
            }

            boolean isNeededPerson;
            for (Person person : persons) {
                isNeededPerson = true;

                if(personModelRequest.firstNamePresent()) {
                    if(!person.getFirstName().equals(personModelRequest.getFirstName())) {
                        isNeededPerson = false;
                    }
                }

                if(personModelRequest.lastNamePresent()) {
                    if(!person.getLastName().equals(personModelRequest.getLastName())) {
                        isNeededPerson = false;
                    }
                }

                if(personModelRequest.agePresent()) {
                    if(!(person.getAge() == personModelRequest.getAge())) {
                        isNeededPerson = false;
                    }
                }

                if(isNeededPerson) {
                    result = gson.toJson(person);
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void add(String request) {
        ObjectMapper objectMapper = new ObjectMapper();
        PersonModelRequest personModelRequest = null;
        Person person = null;
        try {
            personModelRequest = objectMapper.readValue(request, PersonModelRequest.class);

            if(personModelRequest.firstNamePresent() &&
               personModelRequest.lastNamePresent() &&
               personModelRequest.agePresent()) {

                person = new Person(personModelRequest.getFirstName(),
                        personModelRequest.getLastName(),
                        personModelRequest.getAge());

                persons.add(person);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
