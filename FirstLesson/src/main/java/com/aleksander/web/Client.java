package com.aleksander.web;

import com.aleksander.core.services.Server;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Client {

    private static final Logger LOGGER = LoggerFactory.getLogger(Client.class);
    private Server server;

    public Client(Server server) {
        this.server = server;
    }

    public void sendRequestToGet(String request) {
       LOGGER.info(server.get(request));
    }
    
    public void sendRequestToAdd(String request) {
        server.add(request);
    }

}
