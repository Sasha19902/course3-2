package com.aleksander.web;

import com.aleksander.core.model.Person;
import com.aleksander.core.services.Server;
import com.google.gson.Gson;
import org.junit.Test;

public class ClientTest {

    @Test
    public void testAddAndGet() {
        Server server = new Server();
        Client client = new Client(server);
        Gson gson = new Gson();
        Person person1 = new Person("ROB_7", "VER_7", 22);
        Person person2 = new Person("ROB_8", "VER_8", 23);
        Person person3 = new Person("ROB_9", "VER_9", 25);


        client.sendRequestToAdd(gson.toJson(person1));
        client.sendRequestToAdd(gson.toJson(person2));
        client.sendRequestToAdd(gson.toJson(person3));

        client.sendRequestToGet(gson.toJson(person2));
        client.sendRequestToGet(gson.toJson(person3));


        client.sendRequestToGet("{\"firstName\": \"ROB_2\"}");
        client.sendRequestToGet("{\"firstName\": \"ROB_1\"}");

    }
}
